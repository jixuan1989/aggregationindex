package log.analysis.point;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Split {

	public static void main(String[] args) throws IOException {
		File folder =new File("originalData/insertpoint");
		String outputPath="resultdata/writepoint/";
		if(!new File(outputPath).exists()){
			new File(outputPath).mkdirs();
		}
		for(File file:folder.listFiles()){
			BufferedReader reader=new BufferedReader(new FileReader(file));
			String line="";
			Map<Integer, FileWriter> writers=new HashMap<>();
			while((line=reader.readLine())!=null){
				if(line.startsWith("type ")){
					String[] split=line.split(",");
					Integer fname=Integer.valueOf(split[0].substring(line.indexOf("type ")+5));
					if(!writers.containsKey(fname)){
						FileWriter writer=new FileWriter(outputPath+fname);
						writer.write("number, time cost\n");
						writers.put(fname, writer);
					}
					FileWriter writer=writers.get(fname);
					writer.write(split[1]+","+split[2]+"\n");
				}
			}
			reader.close();
			for(FileWriter writer:writers.values()){
				writer.close();
			}
		}
	}

}

package log.analysis.point;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Split4sum {

	public static void main(String[] args) throws IOException {
		File folder =new File("originalData/insertpoint");
		String outputPath="resultdata/writepoint4sum/";
		if(!new File(outputPath).exists()){
			new File(outputPath).mkdirs();
		}
		for(File file:folder.listFiles()){
			BufferedReader reader=new BufferedReader(new FileReader(file));
			String line="";
			Map<Integer, FileWriter> writers=new HashMap<>();
			Map<Integer, Long> totals=new HashMap<>();
			while((line=reader.readLine())!=null){
				if(line.startsWith("type ")){
					String[] split=line.split(",");
					Integer fname=Integer.valueOf(split[0].substring(line.indexOf("type ")+5));
					if(!writers.containsKey(fname)){
						FileWriter writer=new FileWriter(outputPath+fname);
						writer.write("number, time cost\n");
						writers.put(fname, writer);
						totals.put(fname, 0L);
					}
					FileWriter writer=writers.get(fname);
					totals.put(fname,totals.get(fname)+Long.valueOf(split[2]));
					writer.write(split[1]+","+totals.get(fname)+"\n");
					
				}
			}
			reader.close();
			for(FileWriter writer:writers.values()){
				writer.close();
			}
		}
	}

}

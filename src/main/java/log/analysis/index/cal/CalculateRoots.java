package log.analysis.index.cal;

import java.io.FileWriter;
import java.io.IOException;

public class CalculateRoots {

	public static void main(String[] args) throws IOException {
		FileWriter writer=new FileWriter("resultdata/writeindex/rootsize");
		writer.write("package number, root number\n");
		int size=0;
		for(int i=1;i<Math.pow(2, 24);i++){
			size=bitCount2(i);
			writer.write(i+","+size+"\n");
//			System.out.println(i+","+calcuateNumber(i));
		}
		writer.close();
		
	}
	
	public static int bitCount2( int n)
	{
	    int c =0 ;
	    for (c =0; n!=0; ++c)
	    {
	        n &= (n -1) ; // 清除最低位的1
	    }
	    return c ;
	}

}

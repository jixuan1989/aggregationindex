package log.analysis.index.cal;

import java.io.FileWriter;
import java.io.IOException;

public class CalculatePoints {
	public static void main(String[] args) throws IOException {
		FileWriter writer=new FileWriter("resultdata/writeindex/size");
		writer.write("package number, new inserted node number\n");
		FileWriter writer2=new FileWriter("resultdata/writeindex/cumulative_size");
		writer2.write("package number, cumulative node number\n");
		long cumulative=0;
		int size=0;
		for(int i=1;i<Math.pow(2, 20);i++){
			size=calcuateNumber(i);
			cumulative+=size;
			writer.write(i+","+size+"\n");
			writer2.write(i+","+cumulative+"\n");
//			System.out.println(i+","+calcuateNumber(i));
		}
		writer.close();
		writer2.close();
		
	}
	public static int calcuateNumber(long k){
		int mi=is2Power(k);
		while(mi<0){
			k=k-(long)Math.pow(2, -mi);
			mi=is2Power(k);
		}
		return mi+1;
	}
	public static int is2Power(long number){
		int j = 1;
		int i=0;
		while (number>j) {
			j<<=1;
			i++;
		}
		return j==number?i:-(i-1);
	}
}

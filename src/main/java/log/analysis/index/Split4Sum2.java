package log.analysis.index;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Split4Sum2 {

	public static void main(String[] args) throws IOException {
		File folder =new File("originalData/insert");
		String outputPath="resultdata/writeindex4sum2/sum2";
		for(File file:folder.listFiles()){
			BufferedReader reader=new BufferedReader(new FileReader(file));
			String line="";
			FileWriter writer=null;
			FileWriter summaryWriter=null;			
			long size=0;
			long k=1;
			String tmp=null;
			long synopsis=0;
			long index=0;
			while((line=reader.readLine())!=null){
				if(line.contains("begin to write ")){
					String fname=line.substring(line.indexOf("begin to write")+15).replace("[", "-").replace("]", "-").replace(",", "-").replace("(", "-").replace(")", "-");
					File file2=new File(outputPath+fname);
					if(!file2.getParentFile().exists()){
						file2.getParentFile().mkdirs();
					}
					writer=new FileWriter(file2);
					writer.write("point number,total synopsis time cost (total),index time cost (total)\n");
					summaryWriter=new FileWriter(outputPath+fname+"_summary");
					tmp=fname.substring(fname.indexOf("=")+2);
					size=Long.valueOf(tmp.substring(0, tmp.indexOf("_")));
					k=1;
					synopsis=0;
					index=0;
				}
				else if (line.contains("finish writing")){
					if(writer!=null){
						writer.close();
						summaryWriter.close();
					}
					writer=null;
					summaryWriter=null;
				}
				else if(writer!=null){
					if(line.contains(",")){
						String[] details=line.split(",");
						synopsis+=Long.valueOf(details[0]);
						index+=Long.valueOf(details[1])-Long.valueOf(details[0]);
						writer.write((size*k)+","+synopsis+","+index+"\n");
						k++;
					}else{
						summaryWriter.write(line+"\n");
					}
				}
			}
			reader.close();
		}
	}

}

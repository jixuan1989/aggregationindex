package cn.edu.thu.storage;

import cn.edu.thu.storage.cassandra.BackendModelCreator;
import cn.edu.thu.storage.cassandra.BackendReader;
import cn.edu.thu.storage.cassandra.BackendWriter;
import cn.edu.thu.storage.interfaces.IBackendModelCreator;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.memory.FakeByteStore;
import cn.edu.thu.storage.memory.FakeStore;
import cn.edu.thu.storage.mysql.MySQLStore;

public class StorageFactory {
	private static FakeStore fakeStore=new FakeStore();
	private static FakeByteStore fakeByteStore=new FakeByteStore();
	public static IBackendReader getBackaBackendReader(){
		String storageClass=StorageDescriptor.conf.storage_engine;
		switch (storageClass) {
		case "cassandra":
			return new BackendReader();
		case "local":
			return fakeStore;
		case "localbyte":
			return fakeByteStore;
		case "mysql":
			return MySQLStore.getInstance();
		default:
			return new BackendReader();
		}
	}
	public static IBackendWriter getBackaBackendWriter(){
		String storageClass=StorageDescriptor.conf.storage_engine;
		switch (storageClass) {
		case "cassandra":
			return new BackendWriter();
		case "local":
			return fakeStore;
		case "localbyte":
			return fakeByteStore;
		case "mysql":
			return MySQLStore.getInstance();
		default:
			return new BackendWriter();
		}
	}
	public static IBackendModelCreator getBackendModelCreator(){
		String storageClass=StorageDescriptor.conf.storage_engine;
		switch (storageClass) {
		case "cassandra":
			return new BackendModelCreator();
		case "local":
			return fakeStore;
		case "localbyte":
			return fakeByteStore;
		case "mysql":
			return MySQLStore.getInstance();
		default:
			return new BackendModelCreator();
		}
	}
}

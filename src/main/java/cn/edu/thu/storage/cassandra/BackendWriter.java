package cn.edu.thu.storage.cassandra;

import java.nio.ByteBuffer;
import java.util.List;

import org.junit.experimental.theories.DataPoint;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.model.FixFloatPackage;
import cn.edu.thu.storage.model.serializer.DigestSerializeFactory;
import cn.edu.thu.storage.model.serializer.PackageSerializeFactory;


public class BackendWriter implements IBackendWriter
{
	public static String ks = StorageDescriptor.conf.cassandra_keyspace;
	
//	static String DATA_PRE = "s_";
//	static String DIGEST_PRE = "d_";
	
	private static PackageSerializeFactory packageFactory = PackageSerializeFactory.getInstance();
	private static DigestSerializeFactory digestFactory = DigestSerializeFactory.getInstance();
	@Override
	public void write(String key, String cf, long startTimestamp, FixFloatPackage dp)
			throws Exception {
		byte[] bt = packageFactory.serialize(dp);
		ByteBuffer bb = ByteBuffer.wrap(bt);
		CassandraCluster.getInstance().insertData(ks, cf, key, startTimestamp, bb);
	}
	@Override
	public void write(String key, String cf, long startTimestamp, FloatDigest digest)
			throws Exception {
		byte[] bt = digestFactory.serialize(digest);
		ByteBuffer bb = ByteBuffer.wrap(bt);
		CassandraCluster.getInstance().insertDigest(ks,  cf, key, startTimestamp, bb);
	}

	public void write(String key, String cf, long col, byte[] data) throws Exception {
		ByteBuffer bb = ByteBuffer.wrap(data);
		CassandraCluster.getInstance().insertData(ks, cf, key, col, bb);
	}

	

}

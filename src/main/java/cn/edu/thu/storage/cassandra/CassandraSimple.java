package cn.edu.thu.storage.cassandra;

import com.datastax.driver.core.*;

public class CassandraSimple
{
	private Cluster cluster;
	
	public void connect(String node)
	{
		cluster = Cluster.builder().addContactPoint(node).build();
		Metadata metadata = cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n", metadata.getClusterName());
		for (Host host : metadata.getAllHosts())
		{
			System.out.printf("Datatacenter: %s; Host: %s; Rack: %s\n", host.getDatacenter(), host.getAddress(),
					host.getRack());
		}
		Session session = null;
		try
		{
			session = cluster.connect();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return;
		}
		session.execute("create keyspace usertable with replication = {'class':'SimpleStrategy,'replication_factor':1};");
		metadata = cluster.getMetadata();
		if (metadata.getKeyspace("usertable") == null)
			System.out.println("failed");
		else
			System.out.println("suc" + metadata.getKeyspace("usertable").getName());
	}
	
	public void close()
	{
		cluster.close();
	}
	
	public static void main(String[] args)
	{
		CassandraSimple client = new CassandraSimple();
		client.connect("127.0.0.1");
		client.close();
	}
}
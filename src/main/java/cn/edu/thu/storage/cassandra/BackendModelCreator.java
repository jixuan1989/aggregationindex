package cn.edu.thu.storage.cassandra;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.datastax.driver.core.ResultSet;

import cn.edu.thu.storage.interfaces.IBackendModelCreator;
/** 
 * @author lina
 */
public class BackendModelCreator implements IBackendModelCreator
{
	private static String replicaClass="SimpleStrategy";
	// {"keyspace_name": "test","replication_config":{"class" : "SimpleStrategy","replication_factor" :
	// 1},"columnfamilies":[{"columnfamily_name": "s0"},{"columnfamily_name": "s1"},{"columnfamily_name":
	// "s2"},{"columnfamily_name": "s3"},{"columnfamily_name": "s4"},{"columnfamily_name": "s5"},{"columnfamily_name":
	// "s6"},{"columnfamily_name": "s7"},{"columnfamily_name": "s8"},{"columnfamily_name": "s9"}]}
	public void initialize(String ks,int replicaFactor, List<String> columnfamilies) throws Exception
	{
		CassandraCluster.getInstance().createKs(ks, replicaClass,replicaFactor);
		addColumnFamilies(ks, columnfamilies);
	}
	/**
	 * @info no one use it.......but it is preserved for lina's hardwork
	 * @param ks
	 * @param cf
	 * @param colOptions
	 * @throws Exception
	 */
	public void addColumn(String ks ,String cf , Map<String, String> colOptions) throws Exception
	{
		String addColumnCql = "ALTER COLUMNFAMILY %s.%s ADD %s %s;" ;
		Set entries = colOptions.entrySet();
		if(entries != null)
		{
			Iterator iterator = entries.iterator();
			while(iterator.hasNext()) 
			{
				Map.Entry entry =(Entry) iterator.next();
				String columnName = (String) entry.getKey();
				String datatype = (String) entry.getValue();
				ResultSet rSet = CassandraCluster.getInstance().session.execute(String.format(addColumnCql, ks, cf, columnName, datatype));
			}
		}
	}
	/**
	 * @info no one use it.......but it is preserved for lina's hardwork
	 * @param ks
	 * @param cf
	 * @param colOptions
	 * @throws Exception
	 */
	public void dropColumn(String ks ,String cf , Map<String, String> colOptions) throws Exception
	{
		String dropColumnCql = "ALTER COLUMNFAMILY %s.%s DROP  %s;" ;
		Set entries = colOptions.entrySet();
		if(entries != null)
		{
			Iterator iterator = entries.iterator();
			while(iterator.hasNext()) 
			{
				Map.Entry entry =(Entry) iterator.next();
				String columnName = (String) entry.getKey();
				ResultSet rSet = CassandraCluster.getInstance().session.execute(String.format(dropColumnCql, ks, cf, columnName));
			}
		}
	}
	/**
	 * @param ks
	 * @param cf cf will be modified to pre+cf
	 */
	public void addColumnFamily(String ks, String cf){
		try {
			CassandraCluster.getInstance().createCf(ks,cf);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @param ks
	 * @cfs name will be modified to pre+cf
	 */
	public void addColumnFamilies(String ks, List<String> cfs)  {
		//TODO More effective
		for(String cf:cfs){
			addColumnFamily(ks, cf);
		}
	}
	/**
	 * 
	 * @param ks
	 * @param cf cf will be modified bo pre+cf
	 * @return
	 * @throws Exception
	 */
	public boolean checkCf(String ks,String cf) throws Exception{
		return CassandraCluster.getInstance().checkCf(ks, cf);
	}
	
	public static void main(String[] args) {
		BackendModelCreator creator = new BackendModelCreator();
		creator.addColumnFamily("dbcore", "stsensortest");
	}
	@Override
	public void addFloatColumnFamily(String ks, String cf) throws Exception {
		// TODO Auto-generated method stub
		addColumnFamily(ks, cf);
	}
}

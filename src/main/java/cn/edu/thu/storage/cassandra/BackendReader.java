package cn.edu.thu.storage.cassandra;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.model.FixFloatPackage;
import cn.edu.thu.storage.model.serializer.DigestSerializeFactory;
import cn.edu.thu.storage.model.serializer.PackageSerializeFactory;

public class BackendReader implements IBackendReader {
	public static Logger logger = LoggerFactory.getLogger(BackendReader.class);
	public static String ks = StorageDescriptor.conf.cassandra_keyspace;

//	static String DATA_PRE = "s_";
//	static String DIGEST_PRE = "d_";

	private PackageSerializeFactory packageSerializer = PackageSerializeFactory.getInstance();
	private DigestSerializeFactory digestSerializer = DigestSerializeFactory.getInstance();

	private CassandraCluster cluster = null;
	private String digestCf=StorageDescriptor.conf.digest_cf;
	private String dataCf=StorageDescriptor.conf.data_cf;
	
	
	public BackendReader() {
		try {
			cluster = CassandraCluster.getInstance();
		} catch (Exception e) {
			logger.error("Cassandra error : " + e.getMessage());
			e.printStackTrace();
		}
	}

	public FloatDigest getDigest(String key, long timestamp) {
		ResultSet rSet = cluster.select(ks,  digestCf, key, timestamp);
		FloatDigest reFloatDigest = null;
		Row oneRow = rSet.one();
		if (oneRow != null) {
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			reFloatDigest = digestSerializer.deserializeFloatDigest(key, timestamp, bytes);
		}
		return reFloatDigest;
	}

	public FixFloatPackage getPackage(String key, long timestamp) {
		ResultSet rSet = cluster.select(ks,  dataCf, key, timestamp);
		FixFloatPackage reDataPackage = null;
		Row oneRow = rSet.one();
		if (oneRow != null) {
			long time = oneRow.getLong("time");
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			reDataPackage = packageSerializer.deserialize(key, time, bytes);
		}
		return reDataPackage;
	}

	public FloatDigest[] getDigests(String key, long startTime, long endTime) {
		if (startTime > endTime)
			return null;

		List<FloatDigest> reList = new ArrayList<FloatDigest>();
		
		FloatDigest startDigest = getBeforeOrEqualDigest(key, startTime);
		if (startDigest != null && (startDigest.getStartTime() + startDigest.getTimeWindow() > startTime))
			reList.add(startDigest);
		
		ResultSet rSet = cluster.selectRange(ks,  digestCf, key, startTime, endTime);
		for (Row r : rSet) {
			long time = r.getLong("time");
			ByteBuffer br = r.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			FloatDigest reFloatDigest = digestSerializer.deserializeFloatDigest(key, time, bytes);

			reList.add(reFloatDigest);
		}
		return reList.toArray(new FloatDigest[] {});
	}

	public FixFloatPackage[] getPackages(String key, long startTime, long endTime) {
		if (startTime > endTime)
			return null;

		ArrayList<FixFloatPackage> reList = new ArrayList<FixFloatPackage>();

		FixFloatPackage startPackage = getBeforeOrEqualPackage(key, startTime);
		if (startPackage != null && (startPackage.getStartTime() + startPackage.getTimeWindow() > startTime))
			reList.add(startPackage);

		ResultSet rSet = cluster.selectRange(ks,  dataCf, key, startTime, endTime);
		for (Row r : rSet) {
			long time = r.getLong("time");
			ByteBuffer br = r.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			FixFloatPackage reDataPackage = packageSerializer.deserialize(key, time, bytes);

			reList.add(reDataPackage);
		}
		return reList.toArray(new FixFloatPackage[] {});
	}

	@Override
	public FloatDigest[] getDigests(String key, Long[] timestamps) {
		if (timestamps == null || timestamps.length == 0)
			return null;

		ResultSet rSet = cluster.selectMulti(ks,  digestCf, key, timestamps);
		List<FloatDigest> result = new ArrayList<>();
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer byteBuffer = oneRow.getBytes("value");
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes, 0, bytes.length);
			FloatDigest reFloatDigest = digestSerializer.deserializeFloatDigest(key, time, bytes);

			result.add(reFloatDigest);
		}
		return result.toArray(new FloatDigest[] {});
	}

	@Override
	public FixFloatPackage[] getPackages(String key, Long[] timestamps) {
		if (timestamps == null || timestamps.length == 0)
			return null;

		ResultSet rSet = cluster.selectMulti(ks,  dataCf, key, timestamps);
		List<FixFloatPackage> reList = new ArrayList<>();
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer byteBuffer = oneRow.getBytes("value");
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes, 0, bytes.length);
			FixFloatPackage reDataPackage = packageSerializer.deserialize(key, time, bytes);

			reList.add(reDataPackage);
		}
		return reList.toArray(new FixFloatPackage[] {});
	}

	public FloatDigest getBeforeOrEqualDigest(String key, long timestamp) {
		List<Pair<Long, FloatDigest>> reList = getBeforeOrEqualDigests(key, timestamp, 1);
		return (reList.size() == 0) ? null : reList.get(0).right;
	}

	public List<Pair<Long, FloatDigest>> getBeforeOrEqualDigests(String key, long timestamp, int n) {
		List<Pair<Long, FloatDigest>> reList = new ArrayList<Pair<Long, FloatDigest>>();
		if (n == 0)
			return reList;

		ResultSet rSet = cluster.selectNearestBefore(ks,  digestCf, key, timestamp, n);
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer byteBuffer = oneRow.getBytes("value");
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes, 0, bytes.length);
			FloatDigest reFloatDigest = digestSerializer.deserializeFloatDigest(key, time, bytes);

			reList.add(new Pair<Long, FloatDigest>(time, reFloatDigest));
		}
		return reList;
	}

	public FixFloatPackage getBeforeOrEqualPackage(String key, long timestamp) {
		List<Pair<Long, FixFloatPackage>> reList = getBeforeOrEqualPackages(key, timestamp, 1);
		return (reList.size() == 0) ? null : reList.get(0).right;
	}

	public List<Pair<Long, FixFloatPackage>> getBeforeOrEqualPackages(String key, long timestamp, int n) {
		List<Pair<Long, FixFloatPackage>> reList = new ArrayList<Pair<Long, FixFloatPackage>>();
		if (n == 0)
			return reList;

		ResultSet rSet = cluster.selectNearestBefore(ks,  dataCf, key, timestamp, n);
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			FixFloatPackage reDataPackage = packageSerializer.deserialize(key, time, bytes);

			reList.add(new Pair<Long, FixFloatPackage>(time, reDataPackage));
		}

		return reList;
	}

	public FloatDigest getAfterOrEqualDigest(String key, long timestamp) {
		List<Pair<Long, FloatDigest>> reList = getAfterOrEqualDigests(key, timestamp, 1);
		return (reList.size() == 0) ? null : reList.get(0).right;
	}

	public List<Pair<Long, FloatDigest>> getAfterOrEqualDigests(String key, long timestamp, int n) {
		List<Pair<Long, FloatDigest>> reList = new ArrayList<Pair<Long, FloatDigest>>();
		if (n == 0)
			return reList;

		ResultSet rSet = cluster.selectNearestAfter(ks,  digestCf, key, timestamp, n);
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer byteBuffer = oneRow.getBytes("value");
			byte[] bytes = new byte[byteBuffer.remaining()];
			byteBuffer.get(bytes, 0, bytes.length);
			FloatDigest reFloatDigest = digestSerializer.deserializeFloatDigest(key, time, bytes);

			reList.add(new Pair<Long, FloatDigest>(time, reFloatDigest));

		}
		return reList;
	}

	public FixFloatPackage getAfterOrEqualPackage(String key, long timestamp) {
		List<Pair<Long, FixFloatPackage>> reList = getAfterOrEqualPackages(key, timestamp, 1);
		return (reList.size() == 0) ? null : reList.get(0).right;
	}

	public List<Pair<Long, FixFloatPackage>> getAfterOrEqualPackages(String key, long timestamp, int n) {
		List<Pair<Long, FixFloatPackage>> reList = new ArrayList<Pair<Long, FixFloatPackage>>();
		if (n == 0)
			return reList;

		ResultSet rSet = cluster.selectNearestAfter(ks,  dataCf, key, timestamp, n);
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			FixFloatPackage reDataPackage = packageSerializer.deserialize(key, time, bytes);

			reList.add(new Pair<Long, FixFloatPackage>(time, reDataPackage));
		}

		return reList;
	}

	public FloatDigest getLatestDigest(String key) throws Exception {
		List<Pair<Long, FloatDigest>> list = getLatestDigests(key, 1);
		return (list.size() == 0) ? null : list.get(0).right;
	}

	public List<Pair<Long, FloatDigest>> getLatestDigests(String key, int n) {
		List<Pair<Long, FloatDigest>> reList = new ArrayList<Pair<Long, FloatDigest>>();
		if (n == 0)
			return reList;

		ResultSet rSet = cluster.selectLatestN(ks,  digestCf, key, n);
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			FloatDigest reFloatDigest = digestSerializer.deserializeFloatDigest(key, time, bytes);

			reList.add(new Pair<Long, FloatDigest>(time, reFloatDigest));
		}

		return reList;
	}

	public FixFloatPackage getLatestPackage(String key) throws Exception {
		List<Pair<Long, FixFloatPackage>> list = getLatestPackages(key, 1);
		return (list.size() == 0) ? null : list.get(0).right;
	}

	public List<Pair<Long, FixFloatPackage>> getLatestPackages(String key, int n) {
		List<Pair<Long, FixFloatPackage>> reList = new ArrayList<Pair<Long, FixFloatPackage>>();
		if (n == 0)
			return reList;

		ResultSet rSet = cluster.selectLatestN(ks,  dataCf, key, n);
		for (Row oneRow : rSet) {
			long time = oneRow.getLong("time");
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			FixFloatPackage reDataPackage = packageSerializer.deserialize(key, time, bytes);

			reList.add(new Pair<Long, FixFloatPackage>(time, reDataPackage));
		}

		return reList;
	}

	@Override
	public byte[] getBytes(String key, String cf, long startTime) {
		ResultSet rSet = cluster.select(ks,  cf, key, startTime);
		Row oneRow = rSet.one();
		if (oneRow != null) {
			ByteBuffer br = oneRow.getBytes("value");
			byte[] bytes = new byte[br.remaining()];
			br.get(bytes, 0, bytes.length);
			return bytes;
		}
		return null;
	}

	

}

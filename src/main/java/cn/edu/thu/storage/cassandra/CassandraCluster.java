package cn.edu.thu.storage.cassandra;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.querybuilder.QueryBuilder;

import cn.edu.thu.storage.StorageDescriptor;

public class CassandraCluster {
	static Logger logger = LoggerFactory.getLogger(CassandraCluster.class);
	
//	static String DATA_PRE = "s_";
//	static String DIGEST_PRE = "d_";
	
	
	/** !this method MUST be called at least once! */
	public static CassandraCluster getInstance() throws Exception {
		if (myCluster == null)
			myCluster = new CassandraCluster();
		return myCluster;
	}

	protected static CassandraCluster myCluster;
	
	private String[] nodes;
	// one cluster for per phisical
	private Cluster cluster;
	// one session for per ks
	public Session session;
	
	private static String createHotCfCql = "CREATE TABLE IF NOT EXISTS %s.%s (" + "equip text,"
			+ "time bigint," + "value blob," + "PRIMARY KEY(equip,time)" + ");";
	private static String createKsCql = "CREATE KEYSPACE IF NOT EXISTS %s WITH replication = {'class':'%s', 'replication_factor':%d};";
	private static String createCfCql = "CREATE TABLE IF NOT EXISTS %s.%s (" + "equip text," + "time bigint,"
			+ "value blob," + "PRIMARY KEY(equip,time)" + ");";
	private static String deleteCql = "delete value from %s.%s where equip='%s' and time=%d;";
	private static String insertCql = "insert into %s.%s(equip,time,value) values('%s',%d,'%s');";
	private static String selectCql = "select * from %s.%s where equip='%s' and time=%d;";
	private static String selectRangeCql = "select * from %s.%s where equip='%s' and time >= %d and time <= %d;";
	private static String selectLatestCql = "select * from %s.%s where equip='%s' order by time desc limit %d;";
	private static String selectCountCql = "select count(*) from %s.%s where equip ='%s' and time > %d and time <= %d;";
	private static String selectEquipCql = "select distinct equip from %s.%s where token(equip) >= %d and token(equip) <= %d;";
	private static String selectMultiCql = "SELECT * FROM %s.%s WHERE equip='%s' AND time IN ";
	private static String selectNearestBeforeCql = "select * from %s.%s where equip='%s' and time <= %d order by time desc limit %d;";
	private static String selectNearestAfterCql = "select * from %s.%s where equip='%s' and time >= %d order by time asc limit %d;";

	private CassandraCluster() throws Exception {
		nodes = StorageDescriptor.conf.cassandra_nodes.split(",");
		connect(nodes);
	}

	public Session getSession() {
		return session;
	}

	public void connect(String[] nodes) throws Exception {
		if (nodes == null || nodes.length == 0)
			return;
		List<InetAddress> addresses = new ArrayList<InetAddress>();
		for (String node : nodes) {
			try {
				addresses.add(InetAddress.getByName(node));
			} catch (UnknownHostException e) {
				logger.error(e.getMessage());
				throw  e;
			}
		}
		cluster = Cluster.builder().addContactPoints(addresses)
		// .withRetryPolicy(DowngradingConsistencyRetryPolicy.INSTANCE)
		// .withReconnectionPolicy(new ConstantReconnectionPolicy(100L))
				.build();
		logger.info("Try connect cassandra");
		try {
			Metadata metadata = cluster.getMetadata();
			logger.info("Connected to cluster");
			for (Host host : metadata.getAllHosts()) {
				logger.info("Datatacenter: {}; Host: {}; Rack: {}", new Object[] { host.getDatacenter(),
						host.getAddress().toString(), host.getRack() });
			}
			session = cluster.connect();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public void close() {
		session.close();
		cluster.close();
	}

	public void createKs(String ks, String strategy, int replication) throws Exception {
		try {
			session.execute(String.format(createKsCql, ks, strategy, replication));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}

	public void createCf(String ks, String cf) throws Exception {
		try {
			session.execute(String.format(createCfCql, ks,  cf));
			session.execute(String.format(createCfCql, ks,  cf));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public void createHotCf(String ks, String cf) throws Exception {
		try {
			session.execute(String.format(createHotCfCql, ks, cf));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public ResultSet insertData(String ks, String cf, String equip, long time, ByteBuffer value) {
		Statement statement = QueryBuilder.insertInto(ks,  cf).value("equip", equip)
				.value("time", time).value("value", value);
		ResultSet rSet = session.execute(statement);
		return rSet;
	}

	public ResultSet insertDigest(String ks, String cf, String equip, long time, ByteBuffer value) {
		Statement statement = QueryBuilder.insertInto(ks,  cf).value("equip", equip)
				.value("time", time).value("value", value);
		ResultSet rSet = session.execute(statement);
		return rSet;
	}

	public ResultSet select(String ks, String cf, String equip, long time) {
		ResultSet rSet = null;

		logger.info(String.format(selectCql, ks, cf, equip, time));
		rSet = session.execute(String.format(selectCql, ks, cf, equip, time));

		return rSet;
	}

	public ResultSet selectRange(String ks, String cf, String equip, long startTime, long endTime) {
		ResultSet rSet = null;

		logger.info(String.format(selectRangeCql, ks, cf, equip, startTime, endTime));
		rSet = session.execute(String.format(selectRangeCql, ks, cf, equip, startTime, endTime));

		return rSet;
	}

	public ResultSet selectMulti(String ks, String cf, String equip, Long[] timestamps) {
		ResultSet rSet = null;

		StringBuilder stringBuilder = new StringBuilder(String.format(selectMultiCql, ks, cf, equip));
		stringBuilder.append("(");
		for (int i = 0; i < timestamps.length - 1; i++) {
			stringBuilder.append(timestamps[i]);
			stringBuilder.append(",");
		}
		stringBuilder.append(timestamps[timestamps.length - 1] + ")");

		logger.info(String.format(stringBuilder.toString(), ks, cf, equip));
		rSet = session.execute(String.format(stringBuilder.toString(), ks, cf, equip));

		return rSet;
	}

	public ResultSet selectNearestBefore(String ks, String cf, String equip, long timestamp, int n) {
		ResultSet rSet = null;

		logger.debug(String.format(selectNearestBeforeCql, ks, cf, equip, timestamp, n));
		rSet = session.execute(String.format(selectNearestBeforeCql, ks, cf, equip,timestamp, n));

		return rSet;
	}

	public ResultSet selectNearestAfter(String ks, String cf, String equip, long timestamp, int n) {
		ResultSet rSet = null;

		logger.info(String.format(selectNearestAfterCql, ks, cf, equip, timestamp, n));
		rSet = session.execute(String.format(selectNearestAfterCql, ks, cf, equip, timestamp, n));

		return rSet;
	}

	public ResultSet selectLatestN(String ks, String cf, String equip, int n) {
		ResultSet rSet = null;

		logger.info(String.format(selectLatestCql, ks, cf, equip, n));
		rSet = session.execute(String.format(selectLatestCql, ks, cf, equip, n));

		return rSet;
	}

	

	public boolean checkKs(String ks) {
		KeyspaceMetadata ksm = cluster.getMetadata().getKeyspace(ks);
		if (ksm == null)
			return false;
		return true;
	}

	public boolean checkCf(String ks, String cf) {
		KeyspaceMetadata ksm = cluster.getMetadata().getKeyspace(ks);
		if (ksm == null)
			return false;

		TableMetadata tm = ksm.getTable(cf);
		if (tm == null)
			return false;

		return true;
	}

	public ResultSet selectCount(String ks, String cf, String equip, long startTime, long endTime) {
		ResultSet rSet = null;

		logger.debug(String.format(selectCountCql, ks, cf, equip, startTime, endTime));
		rSet = session.execute(String.format(selectCountCql, ks, cf, equip, startTime, endTime));

		return rSet;
	}

	public ResultSet deleteData(String ks, String cf, String equip, String time) {
		ResultSet rSet = null;

		logger.debug(String.format(deleteCql, ks, cf, equip, time));
		rSet = session.execute(String.format(deleteCql, ks, cf, equip, time));

		return rSet;
	}

	 public static void main(String[] args) throws Exception {
	 CassandraCluster.getInstance().createCf("dbcore", "sen95cbc3fdc044");

	 }
}

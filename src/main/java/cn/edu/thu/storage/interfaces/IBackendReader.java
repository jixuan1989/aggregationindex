package cn.edu.thu.storage.interfaces;

import java.util.List;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.model.FixFloatPackage;

public interface IBackendReader
{
	public byte[] getBytes(String key, String cf, long startTime);
	/**
	 * get data digest point by start time, start time is an actual point.
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param startTime
	 * @return a specific digest or null
	 */
	public FloatDigest getDigest(String key, long startTime);

	/**
	 * get data package point by start time, start time is an actual point.
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param startTime
	 * @return a specific datapackage or null
	 */
	public FixFloatPackage getPackage(String key, long startTime);
	
	/**
	 * get range packages by startTime and endTime, the time range of these packages contains query range.
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param startTime
	 * @param endTime
	 * @return packages in the time range, or null
	 */
	public FixFloatPackage[] getPackages(String key, long startTime, long endTime);

	/**
	 * giving some DataPackage's startTime,return these digests from Cassandra in one command
	 * @param dev_id
	 * @param sen_id
	 * @param timeStamps
	 * @return
	 * @throws StorageException 
	 */
	public FloatDigest[] getDigests(String key, Long[] timeStamps);

	/**
	 * giving some DataPackage's actual startTime,return these datapackages
	 * @param dev_id
	 * @param sen_id
	 * @param timeStamps
	 * @return packages in the time range, or null
	 */
	public FixFloatPackage[] getPackages(String key, Long[] timeStamps);

	public FloatDigest getBeforeOrEqualDigest(String key, long timestamp);
	
	/**
	 * get latest n data digests before the timestamp if the timestamp belongs to
	 * a datapackage (not the starttime of a datapackage), the method will
	 * return the datapackage. SO user NEED to decide whether allign the
	 * timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param timestamp
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public List<Pair<Long, FloatDigest>> getBeforeOrEqualDigests(String key,
			long timestamp, int n);
	
	public FixFloatPackage getBeforeOrEqualPackage(String key, long timestamp);
	
	/**
	 * get latest n data packages before the timestamp if the timestamp belongs to
	 * a datapackage (not the starttime of a datapackage), the method will
	 * return the datapackage. SO user NEED to decide whether allign the
	 * timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param timestamp
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public List<Pair<Long, FixFloatPackage>> getBeforeOrEqualPackages(String key,
			long timestamp, int n);

	public FloatDigest getAfterOrEqualDigest(String key, long timestamp);
	
	/**
	 * get latest n data digests after the timestamp if the timestamp belongs to a
	 * datapackage (not the starttime of a datapackage), the method will return
	 * the datapackage. SO user NEED to decide whether allign the timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param timestamp
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public List<Pair<Long, FloatDigest>> getAfterOrEqualDigests(String key,
			long timestamp, int n);
	
	public FixFloatPackage getAfterOrEqualPackage(String key, long timestamp);
	/**
	 * get latest n data packages after the timestamp if the timestamp belongs to a
	 * datapackage (not the starttime of a datapackage), the method will return
	 * the datapackage. SO user NEED to decide whether allign the timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param timestamp
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public List<Pair<Long, FixFloatPackage>> getAfterOrEqualPackages(String key,
			long timestamp, int n);

	/**
	 * get latest one data digest after the timestamp if the timestamp belongs to a
	 * datapackage (not the starttime of a datapackage), the method will return
	 * the datapackage. SO user NEED to decide whether allign the timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public FloatDigest getLatestDigest(String key) throws Exception;
	
	/**
	 * get latest n data digests after the timestamp if the timestamp belongs to a
	 * datapackage (not the starttime of a datapackage), the method will return
	 * the datapackage. SO user NEED to decide whether allign the timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public List<Pair<Long, FloatDigest>> getLatestDigests(String key, int n);
	
	/**
	 * get latest one data package after the timestamp if the timestamp belongs to a
	 * datapackage (not the starttime of a datapackage), the method will return
	 * the datapackage. SO user NEED to decide whether allign the timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public FixFloatPackage getLatestPackage(String key) throws Exception;
	
	/**
	 * get latest n data packages after the timestamp if the timestamp belongs to a
	 * datapackage (not the starttime of a datapackage), the method will return
	 * the datapackage. SO user NEED to decide whether allign the timestamp
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param n
	 * @return
	 * @throws StorageException
	 */
	public List<Pair<Long, FixFloatPackage>> getLatestPackages(String key, int n);
	
}

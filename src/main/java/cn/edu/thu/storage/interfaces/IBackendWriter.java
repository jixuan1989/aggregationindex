package cn.edu.thu.storage.interfaces;

import java.util.List;

import org.junit.experimental.theories.DataPoint;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.model.FixFloatPackage;


public interface IBackendWriter
{
	/**
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param timestamp the timestamp must be a aligned timestamp (a startTime)
	 * @return datapackage or null
	 * @throws Exception
	 */
	/**@param startTimestamp an align time.*/
	public void write(String key, String cf, long startTimestamp, FixFloatPackage dp)
			throws Exception;
	
	/**
	 * 
	 * @param deviceId
	 * @param sensorId
	 * @param timestamp the timestamp must be a aligned timestamp (a startTime)
	 * @return datapackage or null
	 * @throws Exception
	 */
	/**@param startTimestamp an align time.*/
	public void write(String key,String cf, long startTimestamp, FloatDigest digest)
			throws Exception;
	
	public void write(String key,String cf, long startTimestamp, byte[] digest)
			throws Exception;
}

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.edu.thu.storage;

/**
 * A class that contains configuration properties for the cassandra node it runs
 * within.
 * 
 * Properties declared as volatile can be mutated via JMX.
 */
public class StorageConfig {
	private static boolean loadYaml = true;

	public String cassandra_nodes = "127.0.0.1";
	public int sql_cassandra_port = 9042;
	public String cassandra_keyspace = "test";
	public String cassandra_partition_strategy = "SimpleStrategy";
	public int cassandra_replica_factor = 1;
	public String storage_engine = "cassandra";

	public String digest_cf="digest";
	public String data_cf="data";

	public int package_size=1000;
	public int package_frequency=10;

	public String mysql_ip="localhost";
	public int mysql_port=3306;
	public String mysql_user="root";
	public String mysql_pwd="1989hxd";
	
	//time series name
	public String keys="key";
	//how many points in one package
	public String sizes="100";
	//total number of points: Math.pow(2,total)
	public String totals="10";
	
	public String startTimes="0";
	
	public boolean writePackages=true;
	
	public static boolean getLoadYaml() {
		return loadYaml;
	}

	public static void setLoadYaml(boolean value) {
		loadYaml = value;
	}

	public static enum StormMode {
		local, distributed
	}

}

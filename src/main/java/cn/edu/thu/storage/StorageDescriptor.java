/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.edu.thu.storage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Loader;
import org.yaml.snakeyaml.Yaml;

@SuppressWarnings("deprecation")
public class StorageDescriptor
{
    private static final Logger logger = LoggerFactory.getLogger(StorageDescriptor.class);

    public static InetAddress rpcAddress;
     /* Hashing strategy Random or OPHF */
   
    public static StorageConfig conf;
  


    static
    {
        if (StorageConfig.getLoadYaml()) {
			loadYaml();
			loadEnv();
		}
        else
            conf = new StorageConfig();
    }
    public  static void loadYaml()
	{
		String configUrl=null;  	
		InputStream input=null;
		URL url;
		try{
			//in this way, we can read configruations from classpath
			configUrl = System.getProperty("storage.config","/storage.yaml");
			url=StorageDescriptor.class.getResource(configUrl);
//			url = new URL(configUrl);
			logger.info("try to read storage configuration file:"+url);
			url.openStream().close(); // catches well-formed but bogus URLs
			input=url.openStream();
		}catch (Exception e){
			//in this way, we can read configurations from jar.
			configUrl = System.getProperty("storage.config","/storage.yaml");
			logger.error("Loading storage settings from " + configUrl,e);
			//we can get configurations from the jar file in this way.
			input = StorageDescriptor.class.getResourceAsStream(configUrl);
			if(input==null){
				//in this way, we can get configurations from the file system
				try {
					input=new FileInputStream(configUrl);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
		}
		try{
			org.yaml.snakeyaml.constructor.Constructor constructor = new org.yaml.snakeyaml.constructor.Constructor(StorageConfig.class);
			Yaml yaml = new Yaml(new Loader(constructor));
			conf = (StorageConfig)yaml.load(input);
		}catch(Exception e2){
				logger.error("Loading settings storage.yaml failed .",e2);
				System.exit(1);
		}
		finally{
			if(input!=null){
				try {
					input.close();
				} catch (IOException e2) {
				}
			}
		}
	}

	public  static void loadEnv() {
		//System.out.println(System.getenv());

		if (System.getenv("CASSANDRA_NODES") != null) {
			String old = conf.cassandra_nodes;
			conf.cassandra_nodes = System.getenv("CASSANDRA_NODES");
			System.out.println(String.format("override cassandra_nodes %s with %s ", old, conf.cassandra_nodes));
		}

	}

    
  
 
}

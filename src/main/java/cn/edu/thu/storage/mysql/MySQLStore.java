package cn.edu.thu.storage.mysql;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.interfaces.IBackendModelCreator;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.model.FixFloatPackage;
import cn.edu.thu.storage.model.serializer.FixFloatPackageSerializer;
import cn.edu.thu.storage.model.serializer.FloatDigestSerializer;

public class MySQLStore  implements IBackendModelCreator, IBackendWriter,IBackendReader {
	private Connection conn = null;
	private static MySQLStore store=null;
	PreparedStatement write_data_statement;
	PreparedStatement write_digest_statement;
	PreparedStatement read_digest_statement;
	PreparedStatement read_data_statement;
	PreparedStatement read_datas_statement;
//	PreparedStatement read_digests_statement;
	PreparedStatement read_before_equal_digest_statement;
	PreparedStatement read_before_equal_data_statement;
	PreparedStatement read_before_equal_digests_statement;
	PreparedStatement read_before_equal_datas_statement;	
	PreparedStatement read_after_equal_digest_statement;
	PreparedStatement read_after_equal_data_statement;
	PreparedStatement read_after_equal_digests_statement;
	PreparedStatement read_after_equal_datas_statement;	
	PreparedStatement read_latest_data_statement;
	PreparedStatement read_latest_datas_statement;
	PreparedStatement read_latest_digest_statement;
	PreparedStatement read_latest_digests_statement;
	
	
	public static MySQLStore getInstance(){
		if(store==null){
			store=new MySQLStore();
		}
		return store;
	}
	private MySQLStore(){
		String url = "jdbc:mysql://"+StorageDescriptor.conf.mysql_ip+":"+StorageDescriptor.conf.mysql_port+"?"
				+ "user="+StorageDescriptor.conf.mysql_user+"&password="+StorageDescriptor.conf.mysql_pwd+"&useUnicode=true&characterEncoding=UTF8";
		try {
			Class.forName("com.mysql.jdbc.Driver");// 鍔ㄦ�佸姞杞絤ysql椹卞姩
			System.out.println("load MySQL successfully");
			conn = DriverManager.getConnection(url);
			conn.setAutoCommit(true);
			Statement statement =conn.createStatement();
			 statement.execute("use "+StorageDescriptor.conf.cassandra_keyspace);
			
			write_data_statement=conn.prepareStatement("insert into "+StorageDescriptor.conf.data_cf+" (`id`,`time`,`value`) values (?,?,?)");
			write_digest_statement=conn.prepareStatement("insert into "+StorageDescriptor.conf.digest_cf+" (`id`,`time`,`value`) values (?,?,?)");
			 read_digest_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=? and `time`=?");;
			 read_data_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=? and `time`=?");;
			 read_datas_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=? and `time`>=? and `time`<=? limit ?");
//			 read_digests_statement=conn.prepareStatement("select `value` from "+StorageDescriptor.conf.digest_cf+" where `id`=?  and `time`in (?) ");
			 read_before_equal_digest_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=? and `time`<=? order by `time` desc limit 1 ");;
			 read_before_equal_data_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=? and `time`<=? order by `time` desc limit 1 ");;
			 read_before_equal_digests_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=? and `time`<=? order by `time` desc limit ? ");
			 read_before_equal_datas_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=? and `time`<=? order by `time` desc limit ? ");
			 read_after_equal_digest_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=? and `time`>=? order by `time` limit 1 ");;
			 read_after_equal_data_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=? and `time`>=? order by `time` limit 1 ");;
			 read_after_equal_digests_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=? and `time`>=? order by `time` limit ? ");;
			 read_after_equal_datas_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=? and `time`>=? order by `time` limit ? ");;
			 read_latest_data_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=?  order by `time` desc limit 1 ");
			 read_latest_datas_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=?  order by `time` desc limit ? ");
			 read_latest_digest_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=?  order by `time` desc limit 1 ");
			 read_latest_digests_statement=conn.prepareStatement("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`=?  order by `time` desc limit ?");

				
		} catch (SQLException e) {
			System.out.println("MySQL鎿嶄綔閿欒");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	

	@Override
	public FloatDigest getDigest(String key, long startTime) {
		try {
			read_digest_statement.setString(1, key);
			read_digest_statement.setLong(2, startTime);
			ResultSet rset=read_digest_statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return FloatDigestSerializer.getInstance().deserialize(key, startTime, outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FixFloatPackage getPackage(String key, long startTime) {
		try {
			read_data_statement.setString(1, key);
			read_data_statement.setLong(2, startTime);
			ResultSet rset=read_data_statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return FixFloatPackageSerializer.getInstance().deserialize(key, startTime, outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FixFloatPackage[] getPackages(String key, long startTime,
			long endTime) {
		try {
			read_datas_statement.setString(1, key);
			read_datas_statement.setLong(2, startTime);
			read_datas_statement.setLong(3, endTime);
			ResultSet rset=read_datas_statement.executeQuery();
			List<FixFloatPackage> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add( FixFloatPackageSerializer.getInstance().deserialize(key, time, outputStream.toByteArray()));
			}
			return packages.toArray(new FixFloatPackage[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FloatDigest[] getDigests(String key, Long[] timeStamps) {
		StringBuilder sbBuilder=new StringBuilder();
		for(Long time:timeStamps){
			sbBuilder.append(time+",");
		}
		
		String sql=String.format("select `time`,`value` from "+StorageDescriptor.conf.digest_cf+" where `id`='%s'  and `time`in (%s)" , key,sbBuilder.substring(0, sbBuilder.length()-1));
		try {
			Statement statement=conn.createStatement();
			ResultSet rset=statement.executeQuery(sql);
			List<FloatDigest> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add( FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray()));
			}
			return packages.toArray(new FloatDigest[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FixFloatPackage[] getPackages(String key, Long[] timeStamps) {
		StringBuilder sbBuilder=new StringBuilder();
		for(Long time:timeStamps){
			sbBuilder.append(time+",");
		}
		String sql=String.format("select `time`,`value` from "+StorageDescriptor.conf.data_cf+" where `id`=?  and `time`in (?)" , key,sbBuilder.substring(0, sbBuilder.length()-1));
		try {
			Statement statement=conn.createStatement();
			ResultSet rset=statement.executeQuery(sql);
			List<FixFloatPackage> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add( FixFloatPackageSerializer.getInstance().deserialize(key, time, outputStream.toByteArray()));
			}
			return packages.toArray(new FixFloatPackage[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FloatDigest getBeforeOrEqualDigest(String key, long timestamp) {
		PreparedStatement statement=read_before_equal_digest_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			ResultSet rset=statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return  FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Pair<Long, FloatDigest>> getBeforeOrEqualDigests(String key,
			long timestamp, int n) {
		PreparedStatement statement=read_before_equal_digests_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			statement.setInt(3, n);
			ResultSet rset=statement.executeQuery();
			List<Pair<Long,FloatDigest>> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add(new Pair<Long,FloatDigest>(time, FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray())));
			}
			return packages;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FixFloatPackage getBeforeOrEqualPackage(String key, long timestamp) {
		PreparedStatement statement=read_before_equal_data_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			ResultSet rset=statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return  FixFloatPackageSerializer.getInstance().deserialize(key, time,outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Pair<Long, FixFloatPackage>> getBeforeOrEqualPackages(
			String key, long timestamp, int n) {
		PreparedStatement statement=read_before_equal_datas_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			statement.setInt(3, n);
			ResultSet rset=statement.executeQuery();
			List<Pair<Long,FixFloatPackage>> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add(new Pair<Long,FixFloatPackage>(time, FixFloatPackageSerializer.getInstance().deserialize(key, time, outputStream.toByteArray())));
			}
			return packages;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FloatDigest getAfterOrEqualDigest(String key, long timestamp) {
		PreparedStatement statement=read_after_equal_digest_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			ResultSet rset=statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return  FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Pair<Long, FloatDigest>> getAfterOrEqualDigests(String key,
			long timestamp, int n) {
		PreparedStatement statement=read_after_equal_digests_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			statement.setInt(3, n);
			ResultSet rset=statement.executeQuery();
			List<Pair<Long,FloatDigest>> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add(new Pair<Long,FloatDigest>(time, FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray())));
			}
			return packages;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FixFloatPackage getAfterOrEqualPackage(String key, long timestamp) {
		PreparedStatement statement=read_after_equal_data_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			ResultSet rset=statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return  FixFloatPackageSerializer.getInstance().deserialize(key, time, outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Pair<Long, FixFloatPackage>> getAfterOrEqualPackages(String key,
			long timestamp, int n) {
		PreparedStatement statement=read_after_equal_datas_statement;
		try {
			statement.setString(1, key);
			statement.setLong(2, timestamp);
			statement.setInt(3, n);
			ResultSet rset=statement.executeQuery();
			List<Pair<Long,FixFloatPackage>> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add(new Pair<Long,FixFloatPackage>(time, FixFloatPackageSerializer.getInstance().deserialize(key, time, outputStream.toByteArray())));
			}
			return packages;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FloatDigest getLatestDigest(String key) throws Exception {
		PreparedStatement statement=read_latest_digest_statement;
		try {
			statement.setString(1, key);
			ResultSet rset=statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return  FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray());
			}else{
				System.out.println("nothing found by mysql??");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("exception?");
		return null;
	}

	@Override
	public List<Pair<Long, FloatDigest>> getLatestDigests(String key, int n) {
		PreparedStatement statement=read_latest_digests_statement;
		try {
			statement.setString(1, key);
			statement.setInt(2, n);
			ResultSet rset=statement.executeQuery();
			List<Pair<Long,FloatDigest>> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add(new Pair<Long,FloatDigest>(time, FloatDigestSerializer.getInstance().deserialize(key, time, outputStream.toByteArray())));
			}
			return packages;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public FixFloatPackage getLatestPackage(String key) throws Exception {
		PreparedStatement statement=read_latest_data_statement;
		try {
			statement.setString(1, key);
			ResultSet rset=statement.executeQuery();
			if(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				return  FixFloatPackageSerializer.getInstance().deserialize(key, time, outputStream.toByteArray());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Pair<Long, FixFloatPackage>> getLatestPackages(String key, int n) {
		PreparedStatement statement=read_latest_datas_statement;
		try {
			statement.setString(1, key);
			statement.setInt(2, n);
			ResultSet rset=statement.executeQuery();
			List<Pair<Long,FixFloatPackage>> packages=new ArrayList<>();
			while(rset.next()){
				Blob blob=rset.getBlob("value");
				long time= rset.getLong("time");
				InputStream inputStream=blob.getBinaryStream();
				ByteArrayOutputStream outputStream=new ByteArrayOutputStream((int) blob.length());
				IOUtils.copy(inputStream, outputStream);
				packages.add(new Pair<Long,FixFloatPackage>(time, FixFloatPackageSerializer.getInstance().deserialize(key, time,outputStream.toByteArray())));
			}
			return packages;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void write(String key, String cf, long startTimestamp,
			FixFloatPackage dp) throws Exception {
		InputStream inputStream=new ByteArrayInputStream(FixFloatPackageSerializer.getInstance().serialize(dp));
		PreparedStatement statement=null;
		if(cf.equals(StorageDescriptor.conf.data_cf)){
			statement=write_data_statement;
		}else{
			statement=conn.prepareStatement("insert into "+cf +"(`id`,`time`,`value`) values (?,?,?)");
		}
		statement.setString(1, key);
		statement.setLong(2, startTimestamp);
		statement.setBlob(3, inputStream);
		statement.execute();
		if(!cf.equals(StorageDescriptor.conf.data_cf)){
			statement.close();;
		}
	}

	@Override
	public void write(String key, String cf, long startTimestamp,
			FloatDigest digest) throws Exception {
		InputStream inputStream=new ByteArrayInputStream(FloatDigestSerializer.getInstance().serialize(digest));
		PreparedStatement statement=null;
		if(cf.equals(StorageDescriptor.conf.digest_cf)){
			statement=write_digest_statement;
		}else{
			statement=conn.prepareStatement("insert into "+cf +"(`id`,`time`,`value`) values (?,?,?)");
		}
		statement.setString(1, key);
		statement.setLong(2, startTimestamp);
		statement.setBlob(3, inputStream);
		statement.execute();
		if(!cf.equals(StorageDescriptor.conf.digest_cf)){
			statement.close();;
		}
	}

	@Override
	public void write(String key, String cf, long startTimestamp, byte[] digest)
			throws Exception {
		InputStream inputStream=new ByteArrayInputStream(digest);
		PreparedStatement statement=conn.prepareStatement("insert into "+cf +"(`id`,`time`,`value`) values (?,?,?)");
		statement.setString(1, key);
		statement.setLong(2, startTimestamp);
		statement.setBlob(3, inputStream);
		statement.execute();
		statement.close();;
	}

	public void write(String key, String cf, long startTimestamp, float point)
			throws Exception {
		PreparedStatement statement=conn.prepareStatement("insert into "+cf +"(`id`,`time`,`value`) values (?,?,?)");
		statement.setString(1, key);
		statement.setLong(2, startTimestamp);
		statement.setFloat(3, point);
		statement.execute();
		statement.close();;
	}
	@Override
	public void initialize(String ks, int replicaFactor,
			List<String> columnfamilies) throws Exception {
		Statement statement=getInstance().conn.createStatement();
		statement.execute("create database if not exists "+ks );
		statement.execute("use "+ks);
		for(String cf:columnfamilies){
			statement.execute("create table if not exists "+ cf+" (`id` varchar(20) , `time` BIGINT ,`value` longblob, CONSTRAINT PRIMARY KEY (`id`,`time`), index (`id`,`time`))");
		}
		statement.close();
	}

	@Override
	public void addColumnFamily(String ks, String cf) throws Exception {
		Statement statement=getInstance().conn.createStatement();
		statement.execute("use "+ks);
		statement.execute("create table if not exists "+ cf+" (`id` varchar(20) , `time` BIGINT ,`value` longblob, CONSTRAINT PRIMARY KEY (`id`,`time`), index (`id`,`time`))");
		statement.close();
	}

	@Override
	public void addColumnFamilies(String ks, List<String> cfs) throws Exception {
		Statement statement=getInstance().conn.createStatement();
		statement.execute("use "+ks);
		for(String cf:cfs){
			statement.execute("create table if not exists "+ cf+" (`id` varchar(20) , `time` BIGINT ,`value` longblob, CONSTRAINT PRIMARY KEY (`id`,`time`), index (`id`,`time`))");
		}
		statement.close();
	}
	@Override
	public void addFloatColumnFamily(String ks, String cf) throws Exception {
		Statement statement=getInstance().conn.createStatement();
		statement.execute("use "+ks);
		statement.execute("create table if not exists "+ cf+" (`id` varchar(20) , `time` BIGINT ,`value` float, CONSTRAINT PRIMARY KEY (`id`,`time`), index (`id`,`time`))");
		statement.close();
	}
	public List<Object> querySQL(String sql) throws Exception{
		Statement statement=getInstance().conn.createStatement();
		ResultSet rSet=statement.executeQuery(sql);
		List<Object> list=new ArrayList<>();
		while(rSet.next()){
			list.add(rSet.getObject(1));
		}
		statement.close();
		return list;
	}
	@Override
	public byte[] getBytes(String key, String cf, long startTime) {
		// TODO Auto-generated method stub
		return null;
	}
	
}

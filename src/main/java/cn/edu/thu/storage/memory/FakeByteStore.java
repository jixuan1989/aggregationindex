package cn.edu.thu.storage.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.interfaces.IBackendModelCreator;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.model.FixFloatPackage;
/**this fake store has only one cf and one */
import cn.edu.thu.storage.model.serializer.FixFloatPackageSerializer;
import cn.edu.thu.storage.model.serializer.FloatDigestSerializer;
public class FakeByteStore implements IBackendModelCreator, IBackendWriter,IBackendReader {
	Map<String,TreeMap<Long,byte[]>> data=new HashMap<String,TreeMap<Long,byte[]>>();
	Map<String,TreeMap<Long,byte[]>> digests=new HashMap<String,TreeMap<Long,byte[]>>();

	FixFloatPackageSerializer dataSerializer=FixFloatPackageSerializer.getInstance();
	FloatDigestSerializer digestSerializer=FloatDigestSerializer.getInstance();
	@Override
	public void initialize(String ks, int replicaFactor, List<String> columnfamilies) throws Exception {
	}

	@Override
	public void addColumnFamily(String ks, String cf) throws Exception {
	}

	@Override
	public void addColumnFamilies(String ks, List<String> cfs) throws Exception {		
	}

	@Override
	public FloatDigest getDigest(String key, long startTime) {
		SortedMap<Long, byte[]> map=digests.get(key);
		if(map==null) return null;
		if(map.get(startTime)==null) return null;
		return digestSerializer.deserialize(key, startTime, map.get(startTime));
	}

	@Override
	public FixFloatPackage getPackage(String key, long startTime) {
		SortedMap<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		if(map.get(startTime)==null) return null;
		return dataSerializer.deserialize(key, startTime, map.get(startTime));
	}

	@Override
	public FixFloatPackage[] getPackages(String key, long startTime, long endTime) {
		SortedMap<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		SortedMap<Long,byte[]> pks=map.subMap(startTime, endTime);
		if(data.get(key).get(endTime)!=null){
			pks.put(endTime, data.get(key).get(endTime));
		}
		FixFloatPackage[] pkgs=new FixFloatPackage[pks.size()];
		int i=0;
		for(Map.Entry<Long,byte[]> entry:pks.entrySet()){
			pkgs[i++]=dataSerializer.deserialize(key, entry.getKey(), entry.getValue());
		}
		return pkgs;
	}

	@Override
	public FloatDigest[] getDigests(String key, Long[] timeStamps) {
		List<FloatDigest> list=new ArrayList<>();
		Map<Long, byte[]> map=digests.get(key);
		if(map==null) return null;
		byte[] pkg=null;
		for(long time:timeStamps){
			if((pkg=map.get(time))!=null)
			list.add(digestSerializer.deserialize(key, time, pkg));
		}
		return list.toArray(new FloatDigest[]{});
	}

	@Override
	public FixFloatPackage[] getPackages(String key, Long[] timeStamps) {
		List<FixFloatPackage> list=new ArrayList<>();
		Map<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		byte[] pkg=null;
		for(long time:timeStamps){
			if((pkg=map.get(time))!=null)
				list.add(dataSerializer.deserialize(key, time, pkg));
		}
		return list.toArray(new FixFloatPackage[]{});
	}

	@Override
	public FloatDigest getBeforeOrEqualDigest(String key, long timestamp) {
		TreeMap<Long, byte[]> map=digests.get(key);
		if(map==null||map.size()==0) return null;
		Map.Entry<Long, byte[]> entry=map.floorEntry(timestamp);
		if(entry==null) return null;
		return digestSerializer.deserialize(key, entry.getKey(), entry.getValue());
	}

	@Override
	public List<Pair<Long, FloatDigest>> getBeforeOrEqualDigests(String key, long timestamp, int n) {
		List<Pair<Long, FloatDigest>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=digests.get(key);
		if(map==null) return null;
		NavigableMap<Long,byte[]> map2=map.headMap(timestamp, true);
		int i=0;
		for(Map.Entry<Long, byte[]> entry:map2.descendingMap().entrySet()){
			if(i<n){
				list.add(new Pair<Long,FloatDigest>(entry.getKey(),
						digestSerializer.deserialize(key, entry.getKey(), entry.getValue())));
			}
		}
		return list;
	}

	@Override
	public FixFloatPackage getBeforeOrEqualPackage(String key, long timestamp) {
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null||map.size()==0) return null;
		Map.Entry<Long, byte[]> entry=map.floorEntry(timestamp);
		if(entry==null) return null;
		return dataSerializer.deserialize(key, entry.getKey(), entry.getValue());
	}

	@Override
	public List<Pair<Long, FixFloatPackage>> getBeforeOrEqualPackages(String key, long timestamp, int n) {
		List<Pair<Long, FixFloatPackage>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		NavigableMap<Long,byte[]> map2=map.headMap(timestamp, true);
		int i=0;
		for(Map.Entry<Long, byte[]> entry:map2.descendingMap().entrySet()){
			if(i<n){
				list.add(new Pair<Long,FixFloatPackage>(entry.getKey(),
						dataSerializer.deserialize(key, entry.getKey(), entry.getValue())));
			}
		}
		return list;
	}

	@Override
	public FloatDigest getAfterOrEqualDigest(String key, long timestamp) {
		TreeMap<Long, byte[]> map=digests.get(key);
		if(map==null||map.size()==0) return null;
		Map.Entry<Long, byte[]> entry=map.ceilingEntry(timestamp);
		if(entry==null) return null;
		return digestSerializer.deserialize(key, entry.getKey(), entry.getValue());
	}

	@Override
	public List<Pair<Long, FloatDigest>> getAfterOrEqualDigests(String key, long timestamp, int n) {
		List<Pair<Long, FloatDigest>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=digests.get(key);
		if(map==null) return null;
		NavigableMap<Long,byte[]> map2=map.tailMap(timestamp, true);
		int i=0;
		for(Map.Entry<Long, byte[]> entry:map2.entrySet()){
			if(i<n){
				list.add(new Pair<Long,FloatDigest>(entry.getKey(),
						digestSerializer.deserialize(key, entry.getKey(),entry.getValue())));
			}
		}
		return list;
	}

	@Override
	public FixFloatPackage getAfterOrEqualPackage(String key, long timestamp) {
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null||map.size()==0) return null;
		Map.Entry<Long, byte[]> entry=map.ceilingEntry(timestamp);
		if(entry==null) return null;
		return dataSerializer.deserialize(key, entry.getKey(),entry.getValue());
	}

	@Override
	public List<Pair<Long, FixFloatPackage>> getAfterOrEqualPackages(String key, long timestamp, int n) {
		List<Pair<Long, FixFloatPackage>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		NavigableMap<Long,byte[]> map2=map.tailMap(timestamp, true);
		int i=0;
		for(Map.Entry<Long, byte[]> entry:map2.entrySet()){
			if(i<n){
				list.add(new Pair<Long,FixFloatPackage>(entry.getKey(),
						dataSerializer.deserialize(key, entry.getKey(), entry.getValue())));
			}
		}
		return list;
	}

	@Override
	public FloatDigest getLatestDigest(String key) throws Exception {
		List<Pair<Long, FloatDigest>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=digests.get(key);
		if(map==null||map.size()==0) return null;
		return digestSerializer.deserialize(key, map.lastKey(), map.lastEntry().getValue());
	}

	@Override
	public List<Pair<Long, FloatDigest>> getLatestDigests(String key, int n) {
		List<Pair<Long, FloatDigest>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=digests.get(key);
		if(map==null) return null;
		int i=0;
		for(Map.Entry<Long, byte[]> entry:map.descendingMap().entrySet()){
			if(i<n){
				list.add(new Pair<Long,FloatDigest>(entry.getKey(),digestSerializer.deserialize(key, entry.getKey(), entry.getValue())));
			}
		}
		return list;
	}

	@Override
	public FixFloatPackage getLatestPackage(String key) throws Exception {
		List<Pair<Long, FixFloatPackage>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		if(map.size()==0)
			return null;
		return dataSerializer.deserialize(key, map.lastEntry().getKey(),map.lastEntry().getValue());
	}

	@Override
	public List<Pair<Long, FixFloatPackage>> getLatestPackages(String key, int n) {
		List<Pair<Long, FixFloatPackage>> list=new ArrayList<>();
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null) return null;
		int i=0;
		for(Map.Entry<Long, byte[]> entry:map.descendingMap().entrySet()){
			if(i<n){
				list.add(new Pair<Long,FixFloatPackage>(entry.getKey(),dataSerializer.deserialize(key,entry.getKey(),entry.getValue())));
			}
		}
		return list;
	}

	@Override
	public void write(String key, String cf, long startTimestamp, FixFloatPackage dp) throws Exception {
		TreeMap<Long, byte[]> map=data.get(key);
		if(map==null){
			map=new TreeMap<>();
			data.put(key, map);
		}
		map.put(startTimestamp, dataSerializer.serialize(dp));
	}

	@Override
	public void write(String key, String cf, long startTimestamp, FloatDigest digest) throws Exception {
		TreeMap<Long, byte[]> map=this.digests.get(key);
		if(map==null){
			map=new TreeMap<>();
			this.digests.put(key, map);
		}
		map.put(startTimestamp, digestSerializer.serialize(digest));
	}

	@Override
	public void write(String key, String cf, long startTimestamp, byte[] digest) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addFloatColumnFamily(String ks, String cf) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public byte[] getBytes(String key, String cf, long startTime) {
		// TODO Auto-generated method stub
		return null;
	}	

}

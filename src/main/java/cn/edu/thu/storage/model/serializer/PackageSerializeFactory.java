package cn.edu.thu.storage.model.serializer;

import cn.edu.thu.storage.model.FixFloatPackage;

public class PackageSerializeFactory
{
	private final String FIX_FLOAT = "FixFloatPackage";
	
	private static PackageSerializeFactory instance = new PackageSerializeFactory();

	private FixFloatPackageSerializer fixFloatPackageSerializer = FixFloatPackageSerializer.getInstance();

	
	private PackageSerializeFactory() {
	}

	public static PackageSerializeFactory getInstance() {
		return instance;
	}

	public byte[] serialize(FixFloatPackage dataPackage) {
		String className = dataPackage.getClass().getSimpleName();
		switch (className) {
			case FIX_FLOAT:
				return fixFloatPackageSerializer.serialize((FixFloatPackage) dataPackage);
			default:
				return null;
		}
	}

	public FixFloatPackage deserialize(String key, long timestamp, byte[] bytes) {
		return fixFloatPackageSerializer.deserialize(key, timestamp, bytes);
	}
}

package cn.edu.thu.storage.model;



/**
 * Each class implementing {@link DefaultDataValue} MUST have a default
 * Constructor
 * 
 * @author leven
 *
 */
public class DefaultDataValue
{
	public static final float FLOAT_ERROR = 0.01F;
	public static final float EMPTY_FLOAT = Float.MAX_VALUE;
	public static final int EMPTY_INTEGER = Integer.MAX_VALUE;
	public static final byte EMPTY_BOOLEAN = Byte.MAX_VALUE;
	
	
}

package cn.edu.thu.storage.model;

import java.util.Arrays;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.DataDigestUtil;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageDescriptor;


public class FixFloatPackage  {
	protected String key = null;
	protected long startTime = 0L;
	
	private boolean startTimeSetted=false;
	public long getStartTime() {
		return startTime;
	}
	protected int count = 0;
	protected int size = StorageDescriptor.conf.package_size;//TODO
	public double frequency=10;
	protected long timeWindow = 0L;//size/frequency
	
	protected int[] offsets;

	
	private float[] data;

	public FixFloatPackage(String key, double frequency) {
		this.key = key;
		size = StorageDescriptor.conf.package_size;//TODO
		this.frequency=frequency;
		timeWindow=(long) (1000*size/frequency); 
		offsets = new int[size];
		Arrays.fill(offsets, DefaultDataValue.EMPTY_INTEGER);
		data = new float[size];
		Arrays.fill(data, DefaultDataValue.EMPTY_FLOAT);
	}
	public void init(long startTime, int count, int size, int[] offsets) {
		this.startTime = startTime;
		this.count = count;
		this.size = size;
		timeWindow=(long) (size/frequency); 
		this.offsets = offsets;
	}
	
	protected boolean add(int index, float element) {
		data[index] = element;
		return true;
	}

	protected Float remove(int index) {
		Float removeData = data[index];
		data[index] = DefaultDataValue.EMPTY_FLOAT;
		return removeData;
	}

	protected Float get(int index) {
		return data[index];
	}

	public void setData(float[] data) {
		this.data = data;
	}

	public FloatDigest getDigest() {
		return DataDigestUtil.floatDigest(key, this.startTime, this.timeWindow, data);
	}

	public float[] getData() {
		return data;
	}

	public TreeMap<Long, Object> getDatas() {
		Float[] byteData = ArrayUtils.toObject(data);
		return getDatas(byteData);
	}

	
	public boolean add(long timestamp, float value) {
		setStartTime(timestamp);
		
		long normalizedTime = normalizedTime(timestamp);
		int index = getIndex(normalizedTime);
		if (offsets[index] == DefaultDataValue.EMPTY_INTEGER)
			count++;
			
		int offset = getOffset(normalizedTime, index);
		offsets[index] = offset;
		return add(index, value);
	}
	
	protected long normalizedTime(long timestamp) {
		return timestamp % timeWindow;
	}
	
	protected int getIndex(long timestamp) {
		double indexRange = (timestamp + 0.0) / timeWindow * size;
		int index = (int) Math.round(indexRange);
		
		if (index == size)
			--index;
			
		return index;
	}
	
	protected int getOffset(long timestamp, int index) {
		int interval = (int) (timeWindow / size);
		int offset = (int) (timestamp - interval * index);
		
		return offset;
	}
	
	protected long getTimestamp(int offset, int index) {
		int interval = (int) (timeWindow / size);
		long timestamp = startTime + (index * interval + offset);
		return timestamp;
	}
	
	
	public int addRange(long startTime, long endTime, float value) {
		
		setStartTime(startTime);
		
		int start = getIndex(normalizedTime(startTime));
		int startOffest = getOffset(normalizedTime(startTime), start);
		
		int end = getIndex(normalizedTime(endTime));
		int endOffest = getOffset(normalizedTime(endTime), end);
		
		for (int i = start; i <= end; ++i) {
			if (offsets[i] == DefaultDataValue.EMPTY_INTEGER) {
				this.count++;
				offsets[i] = 0;
			}
			
			add(i, value);
		}
		
		offsets[start] = startOffest;
		offsets[end] = endOffest;
		
		return (end - start + 1);
	}
	
	private void setStartTime(long timestamp) {
		if(!startTimeSetted){
			startTime = timestamp - normalizedTime(timestamp);
			startTimeSetted=true;
		}
	}
	
	private void resetStartTime() {
		if (count == 0)
			startTime = 0L;
	}
	
	
	public Object remove(long timestamp) {
		int index = getIndex(normalizedTime(timestamp));
		if (offsets[index] != DefaultDataValue.EMPTY_INTEGER) {
			offsets[index] = DefaultDataValue.EMPTY_INTEGER;
			count--;
		}
		resetStartTime();
		
		return remove(index);
	}
	
	public int removeRange(long startTime, long endTime) {
		int count = 0;
		int start = getIndex(normalizedTime(startTime));
		int end = getIndex(normalizedTime(endTime));
		for (int i = start; i <= end; ++i) {
			if (offsets[i] != DefaultDataValue.EMPTY_INTEGER) {
				offsets[i] = DefaultDataValue.EMPTY_INTEGER;
				this.count--;
			}
			
			remove(i);
			++count;
		}
		resetStartTime();
		
		return count;
	}
	
	
	public Object get(long timestamp) {
		long normalizedTime = normalizedTime(timestamp);
		int index = getIndex(normalizedTime);
		int offset = getOffset(normalizedTime, index);
		if (offset == offsets[index])
			return get(index);
		return null;
	}
	
	
	public FloatDigest getDigest(long startTime, long endTime) {
		Pair<Long, Long> regularRange = rangeRegular(startTime, endTime, this.startTime,this.startTime+this.timeWindow);
		long actStartTime = regularRange.left;
		long actEndTime = regularRange.right;
		
		SortedMap<Long, Object> dataPoints = getDatas().subMap(actStartTime, actEndTime + 1);
		if (null == dataPoints || dataPoints.size() < 1)
			return null;
			
		return DataDigestUtil.getDigest(key, this.startTime, this.timeWindow, dataPoints);
	}
	
	private static Pair<Long, Long> rangeRegular(long queryStartTime, long queryEndTime, long startTime, long endTime) {
		long actStartTime = queryStartTime;
		long actEndTime = queryEndTime;
		
		if (queryStartTime > endTime) {
			actStartTime = endTime;
			actEndTime = endTime;
		}
		else if (queryEndTime < startTime) {
			actStartTime = startTime;
			actEndTime = startTime;
		}
		else {
			if (queryStartTime == -1 || queryStartTime < startTime)
				actStartTime = startTime;
			if (queryEndTime == -1 || queryEndTime > endTime)
				actEndTime = endTime;
				
			if (actEndTime < actStartTime)
				actEndTime = actStartTime;
		}
		
		return new Pair<>(actStartTime, actEndTime);
	}
	
	
	
	
	
	protected TreeMap<Long, Object> getDatas(Object[] data) {
		TreeMap<Long, Object> dataMap = new TreeMap<Long, Object>();
		int offset = 0;
		long timestamp = 0L;
		for (int index = 0; index < offsets.length; index++) {
			if (offsets[index] == DefaultDataValue.EMPTY_INTEGER)
				continue;
			offset = offsets[index];
			timestamp = getTimestamp(offset, index);
			dataMap.put(timestamp, data[index]);
		}
		return dataMap;
	}
	
	public int[] getOffsets() {
		return offsets;
	}
	
	public int size() {
		return size;
	}
	
	public int count() {
		return count;
	}
	
	
	
	
	
	public static boolean isValued(Object objValue) {
		if (objValue instanceof Float) {
			float value = (Float) objValue;
			if (value == DefaultDataValue.EMPTY_FLOAT)
				return false;
		}
		return true;
	}
	public double getFrequence() {
		return this.frequency;
	}
	public long getTimeWindow() {
		return this.timeWindow;
	}
	
	
	
}

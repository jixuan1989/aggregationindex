package cn.edu.thu.storage.model.serializer;

import cn.edu.thu.digest.data.BtreeNode;
import cn.edu.thu.digest.data.DigestNode;
import cn.edu.thu.digest.data.FloatDigest;

public class DigestSerializeFactory
{
	private final String FLOAT = "FloatDigest";

	private final String BTREENODE= "BtreeNode";
	
	private static DigestSerializeFactory instance = new DigestSerializeFactory();

	private FloatDigestSerializer floatSerializer = FloatDigestSerializer.getInstance();
	private BtreeNodeSerializer btreeNodeSerializer = BtreeNodeSerializer.getInstance();
	
	private DigestSerializeFactory() {
	}

	public static DigestSerializeFactory getInstance() {
		return instance;
	}

	public byte[] serialize(DigestNode dataDigest) {
		String className = dataDigest.getClass().getSimpleName();
		switch (className) {
			case FLOAT:
				return floatSerializer.serialize((FloatDigest)dataDigest);
			case BTREENODE:
				 return btreeNodeSerializer.serialize((BtreeNode)dataDigest);
			default:
				return null;
		}
	}

	public FloatDigest deserializeFloatDigest(String key, long timestamp, byte[] bytes) {
			return floatSerializer.deserialize(key, timestamp, bytes);
	}
	public BtreeNode deserializeBtreeNode(String key, long timestamp, byte[] bytes) {
		return btreeNodeSerializer.deserialize(key, timestamp, bytes);
	}
}
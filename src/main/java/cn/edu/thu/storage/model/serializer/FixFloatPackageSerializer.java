package cn.edu.thu.storage.model.serializer;

import java.util.ArrayList;

import cn.edu.thu.digest.utils.MyBytes;
import cn.edu.thu.storage.model.FixFloatPackage;


public class FixFloatPackageSerializer 
{

	protected static FixFloatPackageSerializer instance = new FixFloatPackageSerializer();

	private FixFloatPackageSerializer() {
	}

	public static FixFloatPackageSerializer getInstance() {
		return instance;
	}

	public byte[] serialize(FixFloatPackage dataPackage) {
		//8byte: frequency,
		//4byte: count,
		//4byte: size,
		//8byte: time window,
		//4byte*size: offset,
		//4byte*size: value
		ArrayList<byte[]> byteList = new ArrayList<>();

		byte[] aBytes =null;
		aBytes = MyBytes.doubleToBytes(dataPackage.getFrequence());
		byteList.add(aBytes);
		aBytes = MyBytes.intToBytes(dataPackage.count());
		byteList.add(aBytes);
		aBytes = MyBytes.intToBytes(dataPackage.size());
		byteList.add(aBytes);
		aBytes = MyBytes.longToBytes(dataPackage.getTimeWindow());
		byteList.add(aBytes);
		int[] offsets = dataPackage.getOffsets();
		for (int i = 0; i < offsets.length; ++i) {
			aBytes = MyBytes.intToBytes(offsets[i]);
			byteList.add(aBytes);
		}
		float[] data = dataPackage.getData();
		for (int i = 0; i < data.length; i++) {
			aBytes = MyBytes.floatToBytes(data[i]);
			byteList.add(aBytes);
		}
		return MyBytes.concatByteArrayList(byteList);
	}

	public FixFloatPackage deserialize(String key, long startTime, byte[] bytes) {
		//8byte: frequency,
		//4byte: count,
		//4byte: size,
		//8byte: time window,
		//4byte*size: offset,
		//4byte*size: value
		int position = 0;
		byte[] aBytes = MyBytes.subBytes(bytes, position, 8);
		double frequency = MyBytes.bytesToDouble(aBytes);
		position+=8;
		aBytes = MyBytes.subBytes(bytes, position, 4);
		int count =MyBytes.bytesToInt(aBytes);
		position+=4;
		aBytes = MyBytes.subBytes(bytes, position, 4);
		int size = MyBytes.bytesToInt(aBytes);
		position+=4;
		aBytes = MyBytes.subBytes(bytes, position, 8);
		long timeWindow = MyBytes.bytesToLong(aBytes);
		position+=8;
		int[] offsets = new int[size];
		for (int i = 0; i < size; ++i) {
			aBytes = MyBytes.subBytes(bytes, position, 4);
			offsets[i] = MyBytes.bytesToInt(aBytes);
			position += 4;
		}
		float[] data = new float[size];
		for (int i = 0; i < size; i++) {
			aBytes = MyBytes.subBytes(bytes, position, 4);
			data[i] = MyBytes.bytesToFloat(aBytes);
			position += 4;
		}
		
		FixFloatPackage fixFloatPackage = new FixFloatPackage(key,frequency);
		fixFloatPackage.init(startTime, count, size,  offsets);
		fixFloatPackage.setData(data);

		return fixFloatPackage;
	}

}

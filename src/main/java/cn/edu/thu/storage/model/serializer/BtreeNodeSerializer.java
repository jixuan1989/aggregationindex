package cn.edu.thu.storage.model.serializer;

import java.util.ArrayList;

import cn.edu.thu.digest.data.BtreeNode;
import cn.edu.thu.digest.utils.MyBytes;
import cn.edu.thu.digest.utils.Pair;

public class BtreeNodeSerializer {
	private BtreeNodeSerializer(){
		
	}
	private static BtreeNodeSerializer instance=new BtreeNodeSerializer();
	public static BtreeNodeSerializer getInstance(){
		return instance;
	}
	public byte[] serialize(BtreeNode dataDigest) {
		ArrayList<byte[]> byteList = new ArrayList<>();
		byteList.add(MyBytes.boolToBytes(dataDigest.isLeaf()));
		byteList.add(MyBytes.longToBytes(dataDigest.getName()));
		byteList.add(MyBytes.intToBytes(dataDigest.getSize()));
		byteList.add(MyBytes.longToBytes(dataDigest.getStartTime()));
		byteList.add(MyBytes.longToBytes(dataDigest.getTimeWindow()));
		byteList.add(MyBytes.longToBytes(dataDigest.getInterval()));
		byteList.add(MyBytes.longToBytes(dataDigest.getCount()));
		byteList.add(MyBytes.floatToBytes(dataDigest.getAvg()));
		byteList.add(MyBytes.floatToBytes(dataDigest.getMax()));
		byteList.add(MyBytes.floatToBytes(dataDigest.getMin()));
		if(!dataDigest.isLeaf()){
			for(int i=0;i<dataDigest.getSize();i++){
				byteList.add(MyBytes.longToBytes(dataDigest.getRanges()[i].left));
				byteList.add(MyBytes.longToBytes(dataDigest.getRanges()[i].right));
				byteList.add(MyBytes.longToBytes(dataDigest.getLocations()[i]));
			}
		}
		return MyBytes.concatByteArrayList(byteList);
	}
	@SuppressWarnings("unchecked")
	public BtreeNode deserialize(String key, long timestamp, byte[] bytes) {
		BtreeNode dataDigest=new BtreeNode();
		
		int position =0;
		int offset=0;
		offset=MyBytes.boolSize();
		dataDigest.setLeaf( MyBytes.bytesToBool( MyBytes.subBytes(bytes, position, offset)));
		position+=offset;
		
		offset=MyBytes.longSize();
		dataDigest.setName(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
		position+=offset;
		
		offset=MyBytes.intSize();
		dataDigest.setSize(MyBytes.bytesToInt( MyBytes.subBytes(bytes, position, offset)));	
		position+=offset;
		
		offset=MyBytes.longSize();
		dataDigest.setStartTime(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));	
		position+=offset;
		
		offset=MyBytes.longSize();
		dataDigest.setTimeWindow(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
		position+=offset;
		
		offset=MyBytes.longSize();
		dataDigest.setInterval(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
		position+=offset;
		
		offset=MyBytes.longSize();
		dataDigest.setCount(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));	
		position+=offset;
		
		offset=MyBytes.floatSize();
		dataDigest.setAvg(MyBytes.bytesToFloat( MyBytes.subBytes(bytes, position, offset)));		
		position+=offset;
		
		offset=MyBytes.floatSize();
		dataDigest.setMax(MyBytes.bytesToFloat( MyBytes.subBytes(bytes, position, offset)));		
		position+=offset;
		
		offset=MyBytes.floatSize();
		dataDigest.setMin(MyBytes.bytesToFloat( MyBytes.subBytes(bytes, position, offset)));
		position+=offset;		
		
		if(!dataDigest.isLeaf()){
			dataDigest.setRanges(new Pair[dataDigest.getSize()]);
			dataDigest.setLocations(new long[dataDigest.getSize()]);
			for(int i=0;i<dataDigest.getSize();i++){
				dataDigest.getRanges()[i]=new Pair<Long,Long>(0L,0L);
				offset=MyBytes.longSize();
				dataDigest.getRanges()[i].left=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
				position+=offset;
				
				offset=MyBytes.longSize();
				dataDigest.getRanges()[i].right=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
				position+=offset;
				
				offset=MyBytes.longSize();
				dataDigest.getLocations()[i]=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));	
				position+=offset;
			}
		}
		
		return dataDigest;
	}
}

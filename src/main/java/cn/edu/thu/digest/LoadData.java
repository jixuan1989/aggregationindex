package cn.edu.thu.digest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.index.DigestIndex;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendModelCreator;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.model.FixFloatPackage;

public class LoadData {

	public static void main(String[] args) throws Exception {
		List<String> cfs=new ArrayList<String>();
		cfs.add(StorageDescriptor.conf.digest_cf);
		cfs.add(StorageDescriptor.conf.data_cf);
		IBackendModelCreator schemaCreator=StorageFactory.getBackendModelCreator();
		schemaCreator.initialize(StorageDescriptor.conf.cassandra_keyspace, 
				StorageDescriptor.conf.cassandra_replica_factor, 
				cfs
			);
		
		String[] keys=StorageDescriptor.conf.keys.split(",");

		String[] str_sizes=StorageDescriptor.conf.sizes.split(",");
		int[] sizes=new int[str_sizes.length];
		for(int i=0;i<str_sizes.length;i++){
			sizes[i]=Integer.valueOf(str_sizes[i]);
		}
		str_sizes=StorageDescriptor.conf.totals.split(",");
		long totals[]=new long[str_sizes.length];
		for(int i=0;i<str_sizes.length;i++){
			totals[i]=(long) Math.pow(2,Integer.valueOf(str_sizes[i]));
		}
		str_sizes=StorageDescriptor.conf.startTimes.split(",");
		long starts[]=new long[str_sizes.length];
		for(int i=0;i<str_sizes.length;i++){
			starts[i]= Long.valueOf(str_sizes[i]);
		}

		System.out.println("keys:"+Arrays.toString(keys));
		System.out.println("sizes:"+Arrays.toString(sizes));
		System.out.println("totals:"+Arrays.toString(totals));
		for(int i=0;i<keys.length;i++){
			System.out.println(new Date()+String.format("begin to write [key,size,total,start]=(%s,%d,%d,%d)",keys[i],sizes[i],totals[i],starts[i]));
			load(keys[i],sizes[i],true,totals[i],starts[i]);
			System.out.println(new Date()+String.format("finish writing [key,size,total,start]=(%s,%d,%d,%d)",keys[i],sizes[i],totals[i],starts[i]));
		}
		System.exit(1);
		
		//test read all data.
//		FixFloatPackage[] packages=reader.getPackages(key, 0, total*(1000/StorageDescriptor.conf.package_frequency)+1000);
//		for(FixFloatPackage pkg1:packages){
//			System.out.println("------package:"+pkg1.getStartTime()+"---------");
//			System.out.println(Arrays.toString(pkg1.getData()));
//		}
		//query
//		System.out.println("begin to query.................");
//		long current=System.currentTimeMillis();
//		FloatDigest digest=digestIndex.query(new Pair<Long, Long>(100L, 20000000L));
//		System.out.println("time cost:"+(System.currentTimeMillis()-current));
//		System.out.println(digest.getMax());
	}

	public static void load(String key, int size, boolean includeIndex, long total,long start) throws Exception{
		DigestIndex<FloatDigest> digestIndex=new DigestIndex<>(key);
		IBackendWriter writer=StorageFactory.getBackaBackendWriter();
		long time=start;
		StorageDescriptor.conf.package_size=size;
		FixFloatPackage pkg=null;
		long timeWindow=1000*StorageDescriptor.conf.package_size/StorageDescriptor.conf.package_frequency;
		Random random=new Random();
		int j=0;
		long datatime=0;
		long digesttime=0;
		for(long i=0;i<total;i++){
			if(i% 1000000==0){
				System.out.println("current progress:"+i+":"+((i+0.0)/total));
			}
			long startTime=time-time%timeWindow;
			if(pkg!=null && startTime>pkg.getStartTime()){
				datatime=System.currentTimeMillis();
				writer.write(key, StorageDescriptor.conf.data_cf, pkg.getStartTime(), pkg);
				datatime=System.currentTimeMillis()-datatime;
				j++;
				if(includeIndex){
					FloatDigest digest=pkg.getDigest();
					digesttime=System.currentTimeMillis();
					digestIndex.insert(digest);
					digesttime=System.currentTimeMillis()-digesttime;
				}
				//System.out.println(datatime+","+(datatime+digesttime));
				pkg=null;
			}
			if(pkg==null){
				pkg=new FixFloatPackage(key, StorageDescriptor.conf.package_frequency);				
			}	
			pkg.add(time, random.nextFloat());
			
			time+=1000/StorageDescriptor.conf.package_frequency;
			
		}
		if(pkg!=null){
			datatime=System.currentTimeMillis();
			writer.write(key, StorageDescriptor.conf.data_cf, pkg.getStartTime(), pkg);
			datatime=System.currentTimeMillis()-datatime;
			if(includeIndex){
				FloatDigest digest=pkg.getDigest();
				digesttime=System.currentTimeMillis();
				digestIndex.insert(digest);
				digesttime=System.currentTimeMillis()-digesttime;
			}
			System.out.println(datatime+","+(datatime+digesttime));
			j++;
			System.out.println(j);
		}
		System.out.println("total data points:"+total);
		System.out.println("total package number:"+j);
		System.out.println("max time package:"+pkg.getStartTime());
		System.out.println("total digest node number:"+digestIndex.getMaxSerialNo());

	}
}

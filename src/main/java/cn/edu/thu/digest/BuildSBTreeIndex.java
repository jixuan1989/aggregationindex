package cn.edu.thu.digest;

import cn.edu.thu.digest.index.SBTreeIndex;

public class BuildSBTreeIndex {
	public static void main(String[] args) throws Exception {
		
		if(args.length<6){
			System.out.println("key, starttime, endtime, minInterval, bsize, readFrom");
			return;
		}
		String targetkey=args[0];
		long start=Long.valueOf(args[1]);
		long end=Long.valueOf(args[2]);
		long minInterval=Long.valueOf(args[3]);
		int bsize=Integer.valueOf(args[4]);
		String rowkey=args[5];
		SBTreeIndex index=new SBTreeIndex();
		if(args.length==7){
			System.out.println("debug......");
			index.showAll(targetkey, 1);
		}else{
			index.build(rowkey, targetkey, start, end, bsize, minInterval);
		}
		System.exit(1);
	}
}

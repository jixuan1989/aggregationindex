package cn.edu.thu.digest;

import cn.edu.thu.digest.index.SBTreeIndex;

public class QueryData4SBTree {
	public static void main(String[] args) {
		if(args.length!=4){
			System.out.println("key, starttime, endtime, datakey");
			return;
		}
		String targetkey=args[0];
		long start=Long.valueOf(args[1]);
		long end=Long.valueOf(args[2]);
		String dataKey=args[3];
		SBTreeIndex index=new SBTreeIndex();

		long time =System.currentTimeMillis();
		System.out.println(String.format("will search %d nodes...",index.queryPlan(targetkey,start,end)));
		
		long time2 =System.currentTimeMillis();
		index.queryLeaves(dataKey,start,end);
		time2=System.currentTimeMillis()-time2;
		System.out.println("time cost for read two leaves:"+(time2));
				
		 time =System.currentTimeMillis();
		float result=index.query(targetkey,start,end);
		time=System.currentTimeMillis()-time;
		System.out.println("digest time cost:"+(time));
		System.out.println("total time cost:"+(time+time2));
		
		System.out.println("average value:"+result);
		System.exit(1);
	}
}

package cn.edu.thu.digest;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.index.DigestIndex;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.model.FixFloatPackage;

public class QueryDataBySysnopsis {

	public static void main(String[] args) {
		if(args.length!=3){
			System.out.println("key starttime, endtime");
			return;
		}
		String key=args[0];
		long start=Long.valueOf(args[1]);
		long end=Long.valueOf(args[2]);
		DigestIndex<FloatDigest> digestIndex=new DigestIndex<>(key);
		System.out.println(String.format("will search %d nodes...",digestIndex.queryPlan(new Pair<Long,Long>(start,end))));
		IBackendReader reader=StorageFactory.getBackaBackendReader();
		long time =System.currentTimeMillis();
		FixFloatPackage[] package1=reader.getPackages(key, start, end);
		float result=0;
		for(FixFloatPackage pkg:package1){
			result+=pkg.count();
		}
//		FloatDigest digest=digestIndex.query(new Pair<Long,Long>(start,end));
		System.out.println("time cost:"+(System.currentTimeMillis()-time));
		System.out.println("average value(wrong):"+result);
		System.exit(1);
	}

}

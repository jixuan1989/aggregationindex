package cn.edu.thu.digest.data;

import cn.edu.thu.digest.utils.Pair;

public class BtreeNode  implements DigestNode{
	private long startTime = -1L;
	private long timeWindow = -1L;

	private float max = 0;
	private float min = 0;
	private float avg = 0;
	private long count = 0;
//	private BigDecimal squareSum = new BigDecimal(0.0);

	private boolean leaf=false;
	private long name;
	private int size;
	private Pair<Long, Long> ranges[];
	private long locations[];//the location (column name) of children. internal-node
	private long interval;

	private static long NAME=0;

	
	public static long getUniqueNAME(){
		NAME++;
		return NAME;
	}
	public float getMax() {
		return max;
	}

	public void setMax(float max) {
		this.max = max;
	}

	public float getMin() {
		return min;
	}

	public void setMin(float min) {
		this.min = min;
	}

	public float getAvg() {
		return avg;
	}

	public void setAvg(float avg) {
		this.avg = avg;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	

	public BtreeNode() {
		super();
	}
	@SuppressWarnings("unchecked")
	public BtreeNode( long startTime, long timeWindow,int size, boolean leaf, long name, long locations[]){
		this.startTime=startTime;
		this.timeWindow=timeWindow;
		this.interval=timeWindow/size;
		this.size=size;
		this.ranges=new Pair[size];
		this.leaf=leaf;
		this.name=name;
		if(!leaf){
			this.locations=new long[size];
			long start=startTime;
			for(int i=0;i<size;i++){
				ranges[i]=new Pair<Long,Long>(0L,0L);
				ranges[i].left=start;
				start=start+interval;
				ranges[i].right=start;
				this.locations[i]=locations[i];
			}
		}
	}

	public BtreeNode( long startTime, long timeWindow, float max, float min,
			long count, float avg, int size, boolean leaf, long name,long[] locations) {
		this(startTime,timeWindow,size,leaf,name,locations);
		this.max = max;
		this.min = min;
		this.count = count;
		this.avg = avg;
	}



	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getTimeWindow() {
		return timeWindow;
	}

	public void setTimeWindow(long timeWindow) {
		this.timeWindow = timeWindow;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public long getName() {
		return name;
	}

	public void setName(long name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public Pair<Long, Long>[] getRanges() {
		return ranges;
	}

	public void setRanges(Pair<Long, Long> ranges[]) {
		this.ranges = ranges;
	}

	public long[] getLocations() {
		return locations;
	}

	public void setLocations(long locations[]) {
		this.locations = locations;
	}

	public long getInterval() {
		return interval;
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

}

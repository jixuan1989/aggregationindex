package cn.edu.thu.digest;

import java.util.Scanner;

import cn.edu.thu.digest.index.SBTreeIndex;

public class QueryData4Leaves {
	public static void main(String[] args) {
		String key=args[0];
		Scanner scanner=new Scanner(System.in);
		int command=0;
		long name1=0;
		long name2=0;
		long time;
		SBTreeIndex index=new SBTreeIndex();
		while((command=scanner.nextInt())!=0){
			switch (command) {
			case 1://read two leaves before and after
				System.out.println("read two leaves (before)");
				name1=scanner.nextLong();
				name2=scanner.nextLong();
				time =System.currentTimeMillis();
				index.queryLeaves(key,name1,name2);
				time=System.currentTimeMillis()-time;
				System.out.println("two before time cost for read two leaves:"+(time));
				break;
			case 2://read two leaves
				System.out.println("read two leaves (equals)");
				name1=scanner.nextLong();
				name2=scanner.nextLong();
				time =System.currentTimeMillis();
				index.queryAccuratelyLeaves(key,name1,name2);
				time=System.currentTimeMillis()-time;
				System.out.println("acuurately time cost for read two leaves:"+(time));
				break;
			case 3://read two leaves before and after
				System.out.println("read two digest leaves (before)");
				name1=scanner.nextLong();
				name2=scanner.nextLong();
				time =System.currentTimeMillis();
				index.queryAccuratelyDigestLeaves(key,name1,name2);
				time=System.currentTimeMillis()-time;
				System.out.println("two before time cost for read two leaves:"+(time));
				break;
			case 4://read two leaves
				System.out.println("read two digest leaves (equals)");
				name1=scanner.nextLong();
				name2=scanner.nextLong();
				time =System.currentTimeMillis();
				index.queryDigestLeaves(key,name1,name2);
				time=System.currentTimeMillis()-time;
				System.out.println("acuurately time cost for read two leaves:"+(time));
				break;
					
			default:
				break;
			}
		}
		
		if(args.length!=3){
			System.out.println("key, starttime, endtime");
			return;
		}
		
		long start=Long.valueOf(args[1]);
		long end=Long.valueOf(args[2]);
		

		long time2;
		
		time2 =System.currentTimeMillis();
		index.queryAccuratelyLeaves(key,start,end);
		time2=System.currentTimeMillis()-time2;
		System.out.println("acuurately time cost for read two leaves:"+(time2));
		
		time2=System.currentTimeMillis();
		index.queryLeaves(key,start,end);
		time2=System.currentTimeMillis()-time2;
		System.out.println("time cost for read two leaves:"+(time2));

		time2=System.currentTimeMillis();
		index.queryDigestLeaves(key,start,end);
		time2=System.currentTimeMillis()-time2;
		System.out.println("time cost for read two digest leaves:"+(time2));
		
		
		System.exit(1);
	}
}

package cn.edu.thu.digest;

import java.util.List;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.index.DigestIndex;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.mysql.MySQLStore;

public class QueryData4MySQL {

	public static void main(String[] args) throws Exception {
		if(args.length!=3){
			System.out.println("key starttime, endtime");
			return;
		}
		String key=args[0];
		long start=Long.valueOf(args[1]);
		long end=Long.valueOf(args[2]);
		DigestIndex<FloatDigest> digestIndex=new DigestIndex<>(key);
		System.out.println("load max serial no:"+digestIndex.getMaxSerialNo());
		System.out.println("load roots:"+digestIndex.getRootNodes().size());
		System.out.println(String.format("will search %d nodes...",digestIndex.queryPlan(new Pair<Long,Long>(start,end))));
		MySQLStore store=(MySQLStore) StorageFactory.getBackaBackendReader();
		long time =System.currentTimeMillis();
		List<Object> list=store.querySQL(String.format("select max(value) from points where id='%s' and time>=%d and time <=%d",key,start,end));
		System.out.println("time cost:"+(System.currentTimeMillis()-time));
		System.out.println("average value:"+list.get(0));
		System.exit(1);
	}

}

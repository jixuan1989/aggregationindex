package cn.edu.thu.digest;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.index.DigestIndex;
import cn.edu.thu.digest.utils.Pair;

public class QueryData {

	public static void main(String[] args) {
		if(args.length!=3){
			System.out.println("key starttime, endtime");
			return;
		}
		String key=args[0];
		long start=Long.valueOf(args[1]);
		long end=Long.valueOf(args[2]);
		DigestIndex<FloatDigest> digestIndex=new DigestIndex<>(key);
		long time =System.currentTimeMillis();
		System.out.println(String.format("will search %d nodes...",digestIndex.queryPlan(new Pair<Long,Long>(start,end))));
		time =System.currentTimeMillis();
		FloatDigest digest=digestIndex.query(new Pair<Long,Long>(start,end));
		time=System.currentTimeMillis()-time;
		System.out.println("digest time cost:"+time);
		System.out.println("time cost for read two leaves:"+digestIndex.leafTimeCost);
		System.out.println("total time cost:"+(time+digestIndex.leafTimeCost));
		System.out.println("average value:"+digest.getAvg());
		System.exit(1);
	}

}

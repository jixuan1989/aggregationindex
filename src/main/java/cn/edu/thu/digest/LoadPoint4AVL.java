package cn.edu.thu.digest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.index.AvlTreeIndex;
import cn.edu.thu.digest.index.AvlTreeIndex.Point;
import cn.edu.thu.digest.utils.MyBytes;
import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendModelCreator;
import cn.edu.thu.storage.interfaces.IBackendWriter;

public class LoadPoint4AVL {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		List<String> cfs=new ArrayList<String>();
		cfs.add(StorageDescriptor.conf.digest_cf);
		cfs.add(StorageDescriptor.conf.data_cf);
		IBackendModelCreator schemaCreator=StorageFactory.getBackendModelCreator();
		schemaCreator.addColumnFamily(StorageDescriptor.conf.cassandra_keyspace, "points");
		IBackendWriter writer=StorageFactory.getBackaBackendWriter();
		long time=0;
		Random random=new Random();
		long cost=System.currentTimeMillis();
		long total10=0;
		long total100=0;
		long total1000=0;
		long total10000=0;
		long avltotal10=0;
		long avltotal100=0;
		long avltotal1000=0;
		long avltotal10000=0;		
		
		AvlTreeIndex< Point> t = new AvlTreeIndex< Point>( );  
		
		
		
		for(long i=0L;i<Math.pow(2, 20);i++){
			if(i%10==0){
				System.out.println(String.format("type %d,%d,%d,%d", 10,i,total10,avltotal10));
				total10=0;
			}
			if(i%100==0){
				System.out.println(String.format("type %d,%d,%d,%d", 100,i,total100,avltotal100));
				total100=0;
			}
			if(i%1000==0){
				System.out.println(String.format("type %d,%d,%d,%d", 1000,i,total1000,avltotal1000));
				total1000=0;
			}
			if(i%10000==0){
				System.out.println(String.format("type %d,%d,%d,%d", 10000,i,total10000,avltotal10000));
				total10000=0;
			}
			Point point=new Point();
			point.timpstamp=i;
			point.average=point.max=point.min=random.nextFloat();
			point.count=1;
			
			cost=System.currentTimeMillis();
			writer.write("key", "points", time, MyBytes.floatToBytes(random.nextFloat()));
			cost=System.currentTimeMillis()-cost;
			total10+=cost;
			total100+=cost;
			total1000+=cost;
			total10000+=cost;
			cost=System.currentTimeMillis();
			t.insert( point); 
			cost=System.currentTimeMillis()-cost;
			avltotal10+=cost;
			avltotal100+=cost;
			avltotal1000+=cost;
			avltotal10000+=cost;			
			time+=1000/StorageDescriptor.conf.package_frequency;
			
			
		}
	}

}

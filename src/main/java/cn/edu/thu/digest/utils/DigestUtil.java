package cn.edu.thu.digest.utils;

public class DigestUtil
{
	// if left point, serialNo is the result of getCodeBySerialNum().
	// else, serialNo is the result of getCodeBySerialNum() of its left bother - 1.
	public static long serialToCode(long serialNumber) {
		if (serialNumber % 2 == 1)
			return getCodeBySerialNum(serialNumber);
		else
			return getCodeBySerialNum(serialNumber - 1) + 1;
	}

	/**
	 * Only work in the left leaf of the tree.
	 * @param parentCode
	 * @param interval
	 * @return
	 */
	public static long codeToSerial(long code) {
		long begin = code / 2;
		for (long i = begin;; ++i)
			if (getCodeBySerialNum(i) == code)
				return i;
	}

	public static long serialToParentCode(long serialNumber) {
		return getCodeBySerialNum(serialNumber);
	}

	private static long getCodeBySerialNum(long serialNumber) {
		long count = 0;
		long serialNum = serialNumber;
		for (; serialNum != 0; ++count) {
			serialNum &= (serialNum - 1);
		}
		return (serialNumber << 1) - count;
	}

	public static long getDepth(long interval) {
		long depth = (2 << interval) - 2L;
		return depth;
	}

	public static long getLeftestCode(long parentCode, long rightestCode) {
		long interval = parentCode - rightestCode;
		long leftestCode = parentCode - getDepth(interval);
		return leftestCode;
	}

	private static long[] table2=new long[129];
	static{
		for(int i=0;i<=128;i++){
			table2[i]=(long)Math.pow(2, i)-1;
		}
	}
	public static Pair<Long, Long> getLeftestAndRightestCode(long parentCode){
		int i=0;
		if(parentCode==1){
			return new Pair<Long, Long>(1L,1L);
		}
		long root=parentCode;

		while(true){
			i=0;
			while(i<=128){
				if(root==table2[i]){
					return new Pair<Long, Long>(parentCode-table2[i]+1, parentCode-i+1);
				}
				if(root<table2[i]){
					i--;
					break;
				}else{
					i++;
				}
			}
			root-=table2[i];
		}
	}
	
	public static Pair<Long, Long> getChildrenCode(long parentCode){
		int i=0;
		long root=parentCode;
		int skew=0;
		while(true){
			i=0;
			while(i<=128){
				if(root==1){
					return new Pair<Long, Long>(skew+1L,skew+1L);
				}else if(root==2){
					return new Pair<Long, Long>(skew+2L,skew+2L);
				}
				if(root==table2[i]){
					return new Pair<Long, Long>(root/2+skew, parentCode-1);
				}
				if(root<table2[i]){
					i--;
					break;
				}else{
					i++;
				}
			}
			root-=table2[i];
			skew+=table2[i];
		}
	}
}
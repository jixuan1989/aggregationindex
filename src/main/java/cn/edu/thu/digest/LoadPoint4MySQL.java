package cn.edu.thu.digest;

import java.util.Random;

import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendModelCreator;
import cn.edu.thu.storage.mysql.MySQLStore;

public class LoadPoint4MySQL {

	public static void main(String[] args) throws Exception {
		IBackendModelCreator schemaCreator=StorageFactory.getBackendModelCreator();
		schemaCreator.addFloatColumnFamily(StorageDescriptor.conf.cassandra_keyspace, "points");
		MySQLStore writer=(MySQLStore) StorageFactory.getBackaBackendWriter();
		long time=0;
		Random random=new Random();
		long cost=System.currentTimeMillis();
		long total10=0;
		long total100=0;
		long total1000=0;
		long total10000=0;
		System.out.println("args0: key, args1: 2^i.");
		for(long i=0L;i<Math.pow(2, Integer.valueOf(args[1]));i++){
			if(i%10==0){
				System.out.println(String.format("type %d,%d,%d", 10,i,total10));
				total10=0;
			}
			if(i%100==0){
				System.out.println(String.format("type %d,%d,%d", 100,i,total100));
				total100=0;
			}
			if(i%1000==0){
				System.out.println(String.format("type %d,%d,%d", 1000,i,total1000));
				total1000=0;
			}
			if(i%10000==0){
				System.out.println(String.format("type %d,%d,%d", 10000,i,total10000));
				total10000=0;
			}
			cost=System.currentTimeMillis();
			writer.write(args[0], "points", time, (random.nextFloat()));
			time+=1000/StorageDescriptor.conf.package_frequency;
			cost=System.currentTimeMillis()-cost;
			total10+=cost;
			total100+=cost;
			total1000+=cost;
			total10000+=cost;
		}
	}

}

package cn.edu.thu.digest.index;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendReader;

public class DbcoreDigestIndex  {
	private Logger logger = LoggerFactory.getLogger(DbcoreDigestIndex.class);

	private HashMap<String, DigestIndex> deviceIndexs;


	private IBackendReader reader = StorageFactory.getBackaBackendReader();

	public DbcoreDigestIndex() {}

	public void init(String sensorId) {
		deviceIndexs = new HashMap<String, DigestIndex>();
	}




	public void insert(FloatDigest digest) {
		String deviceId = digest.getKey();
		DigestIndex<FloatDigest> index = deviceIndexs.get(deviceId);
		if (index != null) {
			try {
				index.insert(digest);
			}
			catch (Exception e) {
				logger.error("storage error : {}", e.getMessage());
				e.printStackTrace();
			}
		}
		else {
			try {
				index = new DigestIndex(deviceId);
				deviceIndexs.put(deviceId, index);
			}
			catch (Exception e) {
				logger.error("metadata error : {}", e.getMessage());
				e.printStackTrace();
			}
		}
	}



	public Map getMaximum(String[] deviceIds, long startTime, long endTime) {
		Pair<Long, Long> range = new Pair<Long, Long>(startTime, endTime);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		for (String deviceId : deviceIds) {
			DigestIndex digestIndex = deviceIndexs.get(deviceId);
			FloatDigest floatDigest = (FloatDigest) digestIndex.query(range);
			if (floatDigest != null)
				resultMap.put(deviceId, floatDigest.getMax());

		}
		return resultMap;
	}

	public Map getMinimum(String[] deviceIds, long startTime, long endTime) {
		Pair<Long, Long> range = new Pair<Long, Long>(startTime, endTime);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		for (String deviceId : deviceIds) {
			DigestIndex digestIndex = deviceIndexs.get(deviceId);
			FloatDigest floatDigest = (FloatDigest) digestIndex.query(range);
			if (floatDigest != null)
				resultMap.put(deviceId, floatDigest.getMin());
		}
		return resultMap;
	}

	public Map getCount(String[] deviceIds, long startTime, long endTime) {
		Pair<Long, Long> range = new Pair<Long, Long>(startTime, endTime);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		for (String deviceId : deviceIds) {
			DigestIndex digestIndex = deviceIndexs.get(deviceId);
			FloatDigest floatDigest = (FloatDigest) digestIndex.query(range);
			if (floatDigest != null)
				resultMap.put(deviceId, floatDigest.getCount());
		}
		return resultMap;
	}

	public Map getVariance(String[] deviceIds, long startTime, long endTime) {
		Pair<Long, Long> range = new Pair<Long, Long>(startTime, endTime);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		for (String deviceId : deviceIds) {
			DigestIndex digestIndex = deviceIndexs.get(deviceId);
			FloatDigest floatDigest = (FloatDigest) digestIndex.query(range);
			if (floatDigest != null) {
				BigDecimal count2 = new BigDecimal(floatDigest.getCount());
				double result2 = floatDigest.getSquareSum().divide(count2).doubleValue();
				resultMap.put(deviceId, result2);
			}

		}
		return resultMap;
	}

	public Map getAverage(String[] deviceIds, long startTime, long endTime) {
		Pair<Long, Long> range = new Pair<Long, Long>(startTime, endTime);
		Map<String, Object> resultMap = new HashMap<String, Object>();
		for (String deviceId : deviceIds) {
			DigestIndex digestIndex = deviceIndexs.get(deviceId);
			FloatDigest floatDigest = (FloatDigest) digestIndex.query(range);
			if (floatDigest != null)
				resultMap.put(deviceId, floatDigest.getAvg());

		}
		return resultMap;
	}



}

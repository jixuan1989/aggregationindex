package cn.edu.thu.digest.index;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.thu.digest.data.BtreeNode;
import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.model.FixFloatPackage;
import cn.edu.thu.storage.model.serializer.BtreeNodeSerializer;

public class SBTreeIndex {
	public static String CF_NAME="sbtree";
	private Logger logger = LoggerFactory.getLogger(DigestIndex.class);
	public long leafTimeCost=0;
	protected IBackendReader reader =  StorageFactory.getBackaBackendReader();
	protected IBackendWriter writer = StorageFactory.getBackaBackendWriter();
	protected String rowkey;
	BtreeNodeSerializer serializer=BtreeNodeSerializer.getInstance();
	public void build(String rowkey,String targetkey,  long starttime, long endtime, int bsize, long minInterval) throws Exception{
		FloatDigest digest1=reader.getBeforeOrEqualDigest(rowkey, starttime);
		FloatDigest digest2=reader.getAfterOrEqualDigest(rowkey, endtime);
		long right=digest2.getStartTime();
		long left =digest1.getStartTime();
		long interval=(right-left)/bsize;
		float max=random.nextFloat();
		float min=random.nextFloat();
		int count=random.nextInt(100);
		float avg=random.nextFloat();
		int size=bsize;
		boolean leaf=false;
		long name=BtreeNode.getUniqueNAME();
		long[] locations=null;
		if(interval<=minInterval){
			leaf=true;
		}else{
			locations=new long[size];
			for(int j=0; j<size;j++){
				locations[j]=BtreeNode.getUniqueNAME();
			}
		}
		BtreeNode node=new BtreeNode(left, interval, max, min, count, avg, size, leaf, name, locations);
		save(targetkey,node, minInterval);
	}
	int counter=0;
	Random random=new Random();
	private void save(String targetkey,BtreeNode node, long minInterval) throws Exception{
		byte[] bytes=serializer.serialize(node);
		writer.write(targetkey, CF_NAME, node.getName(), bytes);
		if(!node.isLeaf()){
			for(int i=0;i<node.getSize();i++){
				long startTime=node.getRanges()[i].left;
				long endTime=node.getRanges()[i].right;
				float max=random.nextFloat();
				float min=random.nextFloat();
				int count=random.nextInt(100);
				float avg=random.nextFloat();
				int size=node.getSize();
				boolean leaf=false;
				long name=node.getLocations()[i];
				long[] locations=null;
				if(endTime-startTime<=minInterval){
					leaf=true;
				}else{
					locations=new long[size];
					for(int j=0; j<node.getSize();j++){
						locations[j]=BtreeNode.getUniqueNAME();
					}
				}
				BtreeNode child=new BtreeNode(startTime, endTime-startTime, max, min, count, avg, size, leaf, name, locations);
				counter++;
				if(counter%10000==0){
					System.out.println("debug, save " +child.getName()+",leaf:"+child.isLeaf()+", interval:"+child.getTimeWindow()+",starttime:"+child.getStartTime());
				}
				save(targetkey,child, minInterval);
			}
		}
	}
	public void showAll(String targetKey, long root){
		byte[] bytes=reader.getBytes(targetKey, CF_NAME, root);
		BtreeNode node=serializer.deserialize("", 0, bytes);
		if(!node.isLeaf()){
			System.out.print("node "+node.getName()+",interval:"+node.getTimeWindow()+"\t(children)");
			for(long name:node.getLocations()){
				System.out.print(name+",");
			}
			System.out.println();
			for(long name:node.getLocations()){
				showAll(targetKey, name);
			}
		}else{
			System.out.println("leaf "+node.getName()+",interval:"+node.getTimeWindow());
		}
	}
	public float queryLeaves(String datakey,long start, long end){
		FixFloatPackage left=reader.getBeforeOrEqualPackage(datakey, start);
		FixFloatPackage right=reader.getBeforeOrEqualPackage(datakey, end);
		if(left!=null){
			for(float f:left.getData()){
				;
			}}
		if(right!=null){
			for(float f:right.getData()){
				;
			}
		}
		return 0;
	}
	public float queryAccuratelyLeaves(String datakey,long start, long end){
		FixFloatPackage left=reader.getPackage(datakey, start);
		FixFloatPackage right=reader.getPackage(datakey, end);
		if(left!=null){
			for(float f:left.getData()){
				;
			}}
		if(right!=null){
			for(float f:right.getData()){
				;
			}
		}
		return 0;
	}
	public float queryDigestLeaves(String datakey,long start, long end){
		FloatDigest left=reader.getBeforeOrEqualDigest(datakey, start);
		FloatDigest right=reader.getAfterOrEqualDigest(datakey, end);
		return 0;
	}
	public float queryAccuratelyDigestLeaves(String datakey,long start, long end){
		FloatDigest left=reader.getDigest(datakey, start);
		FloatDigest right=reader.getDigest(datakey, end);
		
		return 0;
	}
	
	public float query(String targetKey, long start, long end){

		byte[] bytes=reader.getBytes(targetKey, CF_NAME, 1);
		BtreeNode root=serializer.deserialize(targetKey, 0, bytes);
		List<BtreeNode> nodes=traverse(targetKey, root, start, end);
		for(BtreeNode node:nodes){
			node.getAvg();
		}
		//we just return a fake result. becaues the count and avg in each node is fake.
		return new Random().nextFloat();
	}
	private List<BtreeNode> traverse(String targetKey,BtreeNode node, long start, long end){
		List<BtreeNode> nodes=new ArrayList<>();
		if(node.isLeaf()){
			nodes.add(node);
		}else{
			for(int i=0;i<node.getSize();i++){
				if(start<=node.getRanges()[i].left&&end>=node.getRanges()[i].right){
					byte[] bytes=reader.getBytes(targetKey, CF_NAME, node.getLocations()[i]);
					BtreeNode child=serializer.deserialize(targetKey, 0, bytes);
					nodes.add(child);
				}else if(start >node.getRanges()[i].right ||end<node.getRanges()[i].left){
				}else{
					byte[] bytes=reader.getBytes(targetKey, CF_NAME, node.getLocations()[i]);
					BtreeNode child=serializer.deserialize(targetKey, 0, bytes);
					nodes.addAll(traverse(targetKey, child, start, end));
				}
			}
		}
		return nodes;
	}
	public int queryPlan(String targetKey, long start, long end){
		byte[] bytes=reader.getBytes(targetKey, CF_NAME, 1);
		BtreeNode root=serializer.deserialize(targetKey, 0, bytes);
		List<BtreeNode> nodes=traverse(targetKey, root, start, end);
		return nodes.size();
	}
}

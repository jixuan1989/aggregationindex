package cn.edu.thu.digest.index;

import java.util.ArrayList;

import cn.edu.thu.digest.data.BtreeNode;
import cn.edu.thu.digest.utils.MyBytes;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;

/**
&nbsp;*二叉平衡树简单实现
&nbsp;*@author kiritor 
&nbsp;*/
public class AvlTreeIndex< T extends AvlTreeIndex.Point>
{
	public String key="avl";//in cassandra
	public static String CF_NAME="avltree";
	public static long NAME=0;
	public static long getUniqueName(){
		NAME++;
		return NAME;
	}

	protected IBackendWriter writer = StorageFactory.getBackaBackendWriter();
	protected IBackendReader reader = StorageFactory.getBackaBackendReader();
	private static class AvlNode< T>{//avl树节点

		AvlNode( T theElement )
		{
			this( theElement, null, null ,getUniqueName());
		}
		AvlNode( T theElement, AvlNode< T> lt, AvlNode< T> rt , long name)
		{
			element  = theElement;
			left     = lt;
			right    = rt;
			height   = 0;
			this.name=name;
		}
		T           element;      // 节点中的数据
		AvlNode< T>  left;         // 左儿子
		AvlNode< T>  right;        // 右儿子
		int         height;       // 节点的高度

		long name=0;// only for cassandra
		long leftname=0;//only for cassandra
		long rightname=0;//only for cassandra
		long fathername=0;//only for cassandra
		byte[] serialize(){
			ArrayList<byte[]> byteList = new ArrayList<>();
			byteList.add(((AvlTreeIndex.Point)element).serialize());
			byteList.add(MyBytes.longToBytes(name));
			byteList.add(MyBytes.longToBytes(leftname));
			byteList.add(MyBytes.longToBytes(rightname));
			byteList.add(MyBytes.longToBytes(fathername));
			byteList.add(MyBytes.intToBytes(this.height));
			return MyBytes.concatByteArrayList(byteList);
		}
		public static AvlNode<Point> deserialize(byte[] bytes){
			Point p=null;
			int position =0;
			int offset=0;
			offset=Point.byteSize();
			p=(Point.deserialize( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;
			AvlNode<Point> node=new AvlNode<AvlTreeIndex.Point>(p);

			offset=MyBytes.longSize();
			node.name=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.longSize();
			node.leftname=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.longSize();
			node.rightname=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.longSize();
			node.fathername=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.intSize();
			node.height=(MyBytes.bytesToInt( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			return node;
		}
	}

	private AvlNode< T> root;//avl树根

	public AvlTreeIndex( )
	{
		root = null;
	}
	//在avl树中插入数据，重复数据复略
	public void insert( T x ) throws Exception
	{
		AvlNode<T> newroot = insert( x, root );
		if(newroot!=root){
			// the first insert, save new root into cassandra
			writer.write(key, CF_NAME, newroot.name, newroot.serialize());
		}
	}

	//在avl中删除数据,这里并未实现
	public void remove( T x )
	{
		System.out.println( "Sorry, remove unimplemented" );
	}

	//在avl树中找最小的数据
	public T findMin( )
	{
		if( isEmpty( ) )
			System.out.println("树空");;
			return findMin( root ).element;
	}
	//在avl树中找最大的数据
	public T findMax( )
	{
		if( isEmpty( ) )
			System.out.println("树空");
		return findMax( root ).element;
	}
	//搜索
	public boolean contains( T x )
	{
		return contains( x, root );
	}

	public void makeEmpty( )
	{
		root = null;
	}

	public boolean isEmpty( )
	{
		return root == null;
	}
	//排序输出avl树
	public void printTree( )
	{
		if( isEmpty( ) )
			System.out.println( "Empty tree" );
		else
			printTree( root );
	}

	int times=0;
	private AvlNode< T> insert( T x, AvlNode< T> t ) throws Exception
	{
		if( t == null ){
			times++;
			AvlNode< T> newt=new AvlNode< T>( x, null, null, getUniqueName() );
			return newt;
		}
		int compareResult = x.compareTo( t.element );

		if( compareResult < 0 )
		{
			t.left = insert( x, t.left );//将x插入左子树中
			if( height( t.left ) - height( t.right ) == 2 ){//打破平衡
				if( x.compareTo( t.left.element ) < 0 )//LL型（左左型）
					t = rotateWithLeftChild( t );
				else   //LR型（左右型）
					t = doubleWithLeftChild( t );
			}else{
				times++;
				//t's children are not changed. but we need to check whether its aggregation are changed. if changed, we need to check whether its father are changed...
				//for average, it must changed.... what a pity...
				while(t.fathername!=0){
					byte[] bb=reader.getBytes(key, CF_NAME, t.fathername);
					writer.write(key, CF_NAME, t.name, bb);
				}
				writer.write(key, CF_NAME, t.name, t.serialize());
			}
		}
		else if( compareResult > 0 )
		{
			t.right = insert( x, t.right );//将x插入右子树中
			if( height( t.right ) - height( t.left ) == 2 ){//打破平衡
				if( x.compareTo( t.right.element ) > 0 )//RR型（右右型）
					t = rotateWithRightChild( t );
				else                           //RL型
					t = doubleWithRightChild( t );
			}else{
				times++;
				while(t.fathername!=0){
					byte[] bb=reader.getBytes(key, CF_NAME, t.fathername);
					writer.write(key, CF_NAME, t.name, bb);
				}
				writer.write(key, CF_NAME, t.name, t.serialize());
			}
		}
		else
			;  // 重复数据，什么也不做
		t.height = Math.max( height( t.left ), height( t.right ) ) + 1;//更新高度
		return t;
	}

	//找最小
	private AvlNode< T> findMin( AvlNode< T> t )
	{
		if( t == null )
			return t;
		while( t.left != null )
			t = t.left;
		return t;
	}
	//找最大
	private AvlNode< T> findMax( AvlNode< T> t )
	{
		if( t == null )
			return t;
		while( t.right != null )
			t = t.right;
		return t;
	}
	//搜索（查找）
	private boolean contains( T x, AvlNode t )
	{
		while( t != null )
		{
			int compareResult = x.compareTo( (T) t.element );

			if( compareResult < 0 )
				t = t.left;
			else if( compareResult > 0 )
				t = t.right;
			else
				return true;    // Match
		}
		return false;   // No match
	}
	//中序遍历avl树
	private void printTree( AvlNode< T> t )
	{
		if( t != null )
		{
			printTree( t.left );
			System.out.println( t.element );
			printTree( t.right );
		}
	}
	//求高度 
	private int height( AvlNode< T> t )
	{
		return t == null ? -1 : t.height;
	}
	//带左子树旋转,适用于LL型
	private AvlNode< T> rotateWithLeftChild( AvlNode< T> k2 ) throws Exception
	{
		AvlNode< T> k1 = k2.left;
		k2.left = k1.right;
		k1.right = k2;
		k2.height = Math.max( height( k2.left ), height( k2.right ) ) + 1;
		k1.height = Math.max( height( k1.left ), k2.height ) + 1;
		times+=k2.height;

		//add
		k1.right.fathername=k1.name;
		k2.left.fathername=k2.name;
		writer.write(key, CF_NAME, k1.rightname, k1.right.serialize());
		writer.write(key, CF_NAME, k2.leftname, k2.left.serialize());
		writer.write(key, CF_NAME, k1.name, k1.serialize());
		writer.write(key, CF_NAME, k2.name, k2.serialize());
		//k1 is father
		while(k1.fathername!=0){
			byte[] bb=reader.getBytes(key, CF_NAME, k1.fathername);
			writer.write(key, CF_NAME, k1.name, bb);
		}
		return k1;
	}
	//带右子树旋转，适用于RR型
	private AvlNode< T> rotateWithRightChild( AvlNode< T> k1 ) throws Exception
	{
		AvlNode< T> k2 = k1.right;
		k1.right = k2.left;
		k2.left = k1;
		k1.height = Math.max( height( k1.left ), height( k1.right ) ) + 1;
		k2.height = Math.max( height( k2.right ), k1.height ) + 1;
		times+=k1.height;

		//add
		k1.right.fathername=k1.name;
		k2.left.fathername=k2.name;
		writer.write(key, CF_NAME, k1.rightname, k1.right.serialize());
		writer.write(key, CF_NAME, k2.leftname, k2.left.serialize());
		writer.write(key, CF_NAME, k1.name, k1.serialize());
		writer.write(key, CF_NAME, k2.name, k2.serialize());
		//k2 is father
		while(k1.fathername!=0){
			byte[] bb=reader.getBytes(key, CF_NAME, k2.fathername);
			writer.write(key, CF_NAME, k2.name, bb);
		}
		return k2;
	}
	//双旋转，适用于LR型
	private AvlNode< T> doubleWithLeftChild( AvlNode< T> k3 ) throws Exception
	{
		k3.left = rotateWithRightChild( k3.left );
		times+=k3.height;
		return rotateWithLeftChild( k3 );
	}
	//双旋转,适用于RL型
	private AvlNode< T> doubleWithRightChild( AvlNode< T> k1 ) throws Exception
	{
		k1.right = rotateWithLeftChild( k1.right );
		times+=k1.height;
		return rotateWithRightChild( k1 );
	}
	// Test program  
//	public static void main( String [ ] args )  
//	{   
//		AvlTreeIndex< Integer> t = new AvlTreeIndex< Integer>( );  
//		final int NUMS = 10*10000;  
//		final int GAP  =   17;  
//		System.out.println( "Checking... (no more output means success)" );  
//		for( int i = GAP; i != 0; i = ( i + GAP ) % NUMS )  
//			t.insert( i );  
//		//t.printTree( );  
//		System.out.println(t.times);
//		System.out.println(t.height(t.root));  
//
//	}  
	public static class Point implements Comparable<Point>{
		public long timpstamp;
		public float max;
		public float min;
		public float average;
		public long count;
		@Override
		public int compareTo(Point o) {
			return Long.compare(this.timpstamp, o.timpstamp);
		}
		public static int byteSize(){
			return MyBytes.longSize()*2+MyBytes.floatSize()*3;
		}
		public byte[] serialize(){
			ArrayList<byte[]> byteList = new ArrayList<>();
			byteList.add(MyBytes.longToBytes(this.timpstamp));
			byteList.add(MyBytes.floatToBytes(this.max));
			byteList.add(MyBytes.floatToBytes(this.min));
			byteList.add(MyBytes.floatToBytes(this.average));
			byteList.add(MyBytes.longToBytes(this.count));
			return MyBytes.concatByteArrayList(byteList);
		}
		public static Point deserialize(byte[] bytes){
			Point p= new Point();
			int position =0;
			int offset=0;
			offset=MyBytes.longSize();
			p.timpstamp=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.floatSize();
			p.max=(MyBytes.bytesToFloat( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.floatSize();
			p.min=(MyBytes.bytesToFloat( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.floatSize();
			p.average=(MyBytes.bytesToFloat( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;

			offset=MyBytes.longSize();
			p.count=(MyBytes.bytesToLong( MyBytes.subBytes(bytes, position, offset)));
			position+=offset;
			return p;
		}

	}
}  

package cn.edu.thu.digest.index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.mysql.fabric.xmlrpc.base.Array;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.DataDigestUtil;
import cn.edu.thu.digest.utils.DigestUtil;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.model.FixFloatPackage;

public class NormalSegmentTreeIndex<T extends FloatDigest> extends DigestIndex<T> {
	public Long root;
	public NormalSegmentTreeIndex(String rowkey) {
		super(rowkey);
		if(this.getRootNodes().size()==1){
			root=getRootNodes().get(0).getCode();
		}else{
			T t=this.getRootNodes().get(0);
			root=2*(t.getCode()+1)-1;
		}
	}
	@SuppressWarnings("unchecked")
	public T query(Pair<Long, Long> range)  {
		if(leftPackage==null)
			leftPackage = reader.getBeforeOrEqualPackage(rowkey, range.left);
		if(rightPackage==null)
			rightPackage = reader.getBeforeOrEqualPackage(rowkey, range.right);
		
		// No packages.
		// xxx xxx left xxx right xxx xxx
		if (leftPackage == null && rightPackage == null)
			return null;
			
		// 1 packages.
		// xxx xxx left ... right xxx xxx
		else if (leftPackage != null && rightPackage != null
				&& leftPackage.getStartTime() == rightPackage.getStartTime())
			return (T) leftPackage.getDigest(range.left, range.right);
			
		List<T> digests = new ArrayList<>();
		
		// query left bound package.
		long leftStartTime = leftPackage.getStartTime();
		T leftDigest = (T) leftPackage.getDigest(leftStartTime, range.left);
		if (null != leftDigest)
			digests.add(leftDigest);
		
		// query right bound package.	
		long rightStartTime = rightPackage.getStartTime();
		T rightDigest = (T) rightPackage.getDigest(rightStartTime, range.right);
		if (null != rightDigest)
			digests.add(rightDigest);
			
		T leftAfterDigest = (T) reader.getAfterOrEqualDigest(rowkey, leftStartTime + 1);
		T rightBeforeDigest = (T) reader.getBeforeOrEqualDigest(rowkey, rightStartTime - 1);

		long leftcode = leftAfterDigest.getCode();
		long rightcode = rightBeforeDigest.getCode();
		System.out.println("leftcode:"+leftcode+",rightcode:"+rightcode+",root:"+root);
		List<Long>parentCodes=search(leftcode, rightcode, root);
		Long[] codeArray = parentCodes.toArray(new Long[0]);
		System.out.println(parentCodes);
		T[] parenetDigests = (T[]) reader.getDigests(rowkey, codeArray);
		List<T> parentNodes = Arrays.asList(parenetDigests);
		digests.addAll(parentNodes);
		T result = (T) DataDigestUtil.aggregate(rowkey, range.left,
				(digests.toArray(new FloatDigest[] {})));
		this.clean();
		return result;
	}
//	public static void main(String[] args){
//		System.out.println(search(2, 27, 16777213));;
//	}
	private  List<Long> search(long left, long right, long root){
		//simulate read disk, though we do not need to read it.
		reader.getDigest(this.rowkey, -root);
		Pair<Long, Long> pair=DigestUtil.getLeftestAndRightestCode(root);
		if(pair.left==pair.right){
			if(pair.left<=right && pair.right>=left)
				return Collections.singletonList(pair.left);
			else 
				return Collections.emptyList();
		}
		if(pair.left>right || pair.right<left)
			return  Collections.emptyList();
		
		List<Long> needed=new ArrayList<>();
		Pair<Long, Long>children=DigestUtil.getChildrenCode(root);
		if(pair.left>=left && pair.right<=right){
			needed.add(-root);
		}else{
			if(pair.left<=right){
				 needed.addAll(search(left, right, children.left));
			}
			if(pair.right>=left){
				needed.addAll(search(left, right, children.right));
			}
		}
		return needed;
	}
	private  int searchPlan(long left, long right, long root){
		//simulate read disk, though we do not need to read it.
		int k=1;//read root
		Pair<Long, Long> pair=DigestUtil.getLeftestAndRightestCode(root);
		if(pair.left==pair.right){
			if(pair.left<=right && pair.right>=left)
				return k;
			else 
				return 0;
		}
		if(pair.left>right || pair.right<left)
			return  0;
		Pair<Long, Long>children=DigestUtil.getChildrenCode(root);
		if(pair.left>=left && pair.right<=right){
			k++;
		}else{
			if(pair.left<=right){
				 k+=(searchPlan(left, right, children.left));
			}
			if(pair.right>=left){
				k+=(searchPlan(left, right, children.right));
			}
		}
		return k;
	}
	@SuppressWarnings("unchecked")
	public int queryPlan(Pair<Long, Long> range){
		long time =System.currentTimeMillis();
		leftPackage = reader.getBeforeOrEqualPackage(rowkey, range.left);
		rightPackage = reader.getBeforeOrEqualPackage(rowkey, range.right);
		 this.leafTimeCost=System.currentTimeMillis()-time;
		 
		// No packages.
		// xxx xxx left xxx right xxx xxx
		if (leftPackage == null && rightPackage == null)
			return 0;
			
		// 1 packages.
		// xxx xxx left ... right xxx xxx
		else if (leftPackage != null && rightPackage != null
				&& leftPackage.getStartTime() == rightPackage.getStartTime())
			return 1;
			
		List<T> digests = new ArrayList<>();
		
		// query left bound package.
		long leftStartTime = leftPackage.getStartTime();
		T leftDigest = (T) leftPackage.getDigest(leftStartTime, range.left);
		if (null != leftDigest)
			digests.add(leftDigest);
		
		// query right bound package.	
		long rightStartTime = rightPackage.getStartTime();
		T rightDigest = (T) rightPackage.getDigest(rightStartTime, range.right);
		if (null != rightDigest)
			digests.add(rightDigest);
			
		T leftAfterDigest = (T) reader.getAfterOrEqualDigest(rowkey, leftStartTime + 1);
		T rightBeforeDigest = (T) reader.getBeforeOrEqualDigest(rowkey, rightStartTime - 1);

		long leftcode = leftAfterDigest.getCode();
		long rightcode = rightBeforeDigest.getCode();
		int k=searchPlan(leftcode, rightcode, root);
		
		return k+2;
	}
}

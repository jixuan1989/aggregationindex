package cn.edu.thu.digest.index;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.edu.thu.digest.data.FloatDigest;
import cn.edu.thu.digest.utils.DataDigestUtil;
import cn.edu.thu.digest.utils.DigestUtil;
import cn.edu.thu.digest.utils.ForestRootStack;
import cn.edu.thu.digest.utils.Pair;
import cn.edu.thu.storage.StorageDescriptor;
import cn.edu.thu.storage.StorageFactory;
import cn.edu.thu.storage.interfaces.IBackendReader;
import cn.edu.thu.storage.interfaces.IBackendWriter;
import cn.edu.thu.storage.model.FixFloatPackage;




public class DigestIndex<T extends FloatDigest> {
	private Logger logger = LoggerFactory.getLogger(DigestIndex.class);
	private long maxSerialNo = 0;
	public long getMaxSerialNo() {
		return maxSerialNo;
	}
	public long leafTimeCost=0;
	private ForestRootStack<T> rootNodes;
	protected IBackendReader reader =  StorageFactory.getBackaBackendReader();
	protected IBackendWriter writer = StorageFactory.getBackaBackendWriter();
	protected String rowkey;
	public DigestIndex(String rowkey) {
		this.rowkey=rowkey;
		T lastDisgest = getLastestDigest();
		if (null == lastDisgest) {
			maxSerialNo = 0;
			rootNodes = new ForestRootStack<T>();
		}
		else {
			maxSerialNo = lastDisgest.getSerial();
			rootNodes = getRoots(lastDisgest);
		}
	}
	
	@SuppressWarnings("unchecked")
	protected T getLastestDigest() {
		T lastestDigest = null;
		try {
			System.out.println("rowkey:"+ rowkey);
			lastestDigest = (T) reader.getLatestDigest(rowkey);
		}
		catch (Exception e) {
			e.printStackTrace();
			logger.error(String.format("Read lastest digest error : (%s)", rowkey));
		}
		return lastestDigest;
	}
	
	@SuppressWarnings("unchecked")
	protected ForestRootStack<T> getRoots(T digest) {
		ForestRootStack<T> rootStack = new ForestRootStack<T>();
		
		List<Long> rootCodes = getRootCodes(maxSerialNo);
		Long[] codeArray = rootCodes.toArray(new Long[] {});
		if(codeArray.length!=0){
			T[] rootArray = (T[]) reader.getDigests(rowkey, codeArray);
			
			for (int i = rootArray.length - 1; i >= 0; --i)
				rootStack.push(rootArray[i]);
		}
		if (digest.getSerial() % 2 == 1)
			rootStack.push(digest);
			
		return rootStack;
	}
	
	protected List<Long> getRootCodes(long serialNo) {
		List<Long> rootCodes = new ArrayList<Long>();
		long tmpUpperSerial = serialNo;
		
		if (tmpUpperSerial % 2 == 1)
			tmpUpperSerial--;
			
		while (tmpUpperSerial > 1) {
			long rightestCode = DigestUtil.serialToCode(tmpUpperSerial);
			long parentCode = DigestUtil.serialToParentCode(tmpUpperSerial);
			rootCodes.add(-parentCode);
			long leftestCode = DigestUtil.getLeftestCode(parentCode, rightestCode);
			tmpUpperSerial = DigestUtil.codeToSerial(leftestCode) - 1;
		}
		
		return rootCodes;
	}
	
	public int insert(T digest) throws Exception {
		++maxSerialNo;
		digest.setSerial(maxSerialNo);
		
		long code = DigestUtil.serialToCode(maxSerialNo);
		digest.setCode(code);
		
		List<T> digests = new ArrayList<>();
		rootNodes.push(digest);
		digests.add(digest);
		
		logger.debug("Insert Node code number : " + code);
		if (maxSerialNo % 2 == 0) {
			long parentCode = DigestUtil.serialToParentCode(maxSerialNo);
			List<T> parentNodes = generateParents(code + 1, parentCode);
			digests.addAll(parentNodes);
		}
		for (T digestNode : digests) {
			writer.write(rowkey,StorageDescriptor.conf.digest_cf, digestNode.getStartTime(),
					 digestNode);
		}
		return digests.size();
	}
	
	private List<T> generateParents(long lowCodeNum, long upCodeNum) {
		List<T> digests = new ArrayList<>();
		
		for (long count = lowCodeNum; count <= upCodeNum; ++count) {
			Pair<T, T> children = rootNodes.popPair();
			T parentNode = generateParent(children.left, children.right);
			logger.debug("Generate parent code number : " + parentNode.getCode());
			rootNodes.push(parentNode);
			digests.add(parentNode);
		}
		
		return digests;
	}
	
	@SuppressWarnings("unchecked")
	private T generateParent(T a, T b) {
		return (T) a.generateParent(a, b);
	}
	
	FixFloatPackage leftPackage ;
	FixFloatPackage rightPackage ;
	public void clean(){
		this.leftPackage=null;
		this.rightPackage=null;
	}
	/**return how many nodes we will read*/
	public int queryPlan(Pair<Long, Long> range){
		long time=System.currentTimeMillis();
		 leftPackage = reader.getBeforeOrEqualPackage(rowkey, range.left);
		 rightPackage = reader.getBeforeOrEqualPackage(rowkey, range.right);
		 this.leafTimeCost=System.currentTimeMillis()-time;
		 // No packages.
		// xxx xxx left xxx right xxx xxx
		if (leftPackage == null && rightPackage == null)
			return 0;
			
		// 1 packages.
		// xxx xxx left ... right xxx xxx
		else if (leftPackage != null && rightPackage != null
				&& leftPackage.getStartTime() == rightPackage.getStartTime())
			return 1;
		List<T> digests = new ArrayList<>();
		
		// query left bound package.
		long leftStartTime = leftPackage.getStartTime();
		T leftDigest = (T) leftPackage.getDigest(leftStartTime, range.left);
		if (null != leftDigest)
			digests.add(leftDigest);
		
		// query right bound package.	
		long rightStartTime = rightPackage.getStartTime();
		T rightDigest = (T) rightPackage.getDigest(rightStartTime, range.right);
		if (null != rightDigest)
			digests.add(rightDigest);
			
		T leftAfterDigest = (T) reader.getAfterOrEqualDigest(rowkey, leftStartTime + 1);
		T rightBeforeDigest = (T) reader.getBeforeOrEqualDigest(rowkey, rightStartTime - 1);
		
		long downSerial = leftAfterDigest.getSerial();
		long upSerial = rightBeforeDigest.getSerial();
		logger.debug("Query range(Serial Number) : " + downSerial + "-" + upSerial);
		Pair<Long, Long> bounds = new Pair<>(downSerial, upSerial);
		if (downSerial % 2 == 0) {
			digests.add(leftAfterDigest);
			bounds.left = bounds.left + 1;
		}
		if (upSerial % 2 == 1) {
			digests.add(rightBeforeDigest);
			bounds.right = bounds.right - 1;
		}
		if (bounds.left < bounds.right) {
			T[] parentArray = queryBetween(bounds);
			List<T> parentNodes = Arrays.asList(parentArray);
			digests.addAll(parentNodes);
		}
		return digests.size();
	}
	
	/**
	 * think TW
	 * 
	 * @param range
	 * @return digest
	 */
	@SuppressWarnings("unchecked")
	public T query(Pair<Long, Long> range)  {
		if(leftPackage==null)
		 leftPackage = reader.getBeforeOrEqualPackage(rowkey, range.left);
		if(rightPackage==null)
		 rightPackage = reader.getBeforeOrEqualPackage(rowkey, range.right);
		
		// No packages.
		// xxx xxx left xxx right xxx xxx
		if (leftPackage == null && rightPackage == null)
			return null;
			
		// 1 packages.
		// xxx xxx left ... right xxx xxx
		else if (leftPackage != null && rightPackage != null
				&& leftPackage.getStartTime() == rightPackage.getStartTime())
			return (T) leftPackage.getDigest(range.left, range.right);
			
		List<T> digests = new ArrayList<>();
		
		// query left bound package.
		long leftStartTime = leftPackage.getStartTime();
		T leftDigest = (T) leftPackage.getDigest(leftStartTime, range.left);
		if (null != leftDigest)
			digests.add(leftDigest);
		
		// query right bound package.	
		long rightStartTime = rightPackage.getStartTime();
		T rightDigest = (T) rightPackage.getDigest(rightStartTime, range.right);
		if (null != rightDigest)
			digests.add(rightDigest);
		long time =System.currentTimeMillis();
		T leftAfterDigest = (T) reader.getAfterOrEqualDigest(rowkey, leftStartTime + 1);
		T rightBeforeDigest = (T) reader.getBeforeOrEqualDigest(rowkey, rightStartTime - 1);
		System.out.println("this step time cost:"+(System.currentTimeMillis()-time));
		long downSerial = leftAfterDigest.getSerial();
		long upSerial = rightBeforeDigest.getSerial();
		logger.debug("Query range(Serial Number) : " + downSerial + "-" + upSerial);
		Pair<Long, Long> bounds = new Pair<>(downSerial, upSerial);
		if (downSerial % 2 == 0) {
			digests.add(leftAfterDigest);
			bounds.left = bounds.left + 1;
		}
		if (upSerial % 2 == 1) {
			digests.add(rightBeforeDigest);
			bounds.right = bounds.right - 1;
		}
		if (bounds.left < bounds.right) {
			T[] parentArray = queryBetween(bounds);
			List<T> parentNodes = Arrays.asList(parentArray);
			digests.addAll(parentNodes);
		}
		
		T result = (T) DataDigestUtil.aggregate(rowkey, range.left,
				(digests.toArray(new FloatDigest[] {})));
		this.clean();	
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public T[] queryBetween(Pair<Long, Long> range) {
		long lowerSerial = range.left;
		long upperSerial = range.right;
		
		long upperCode = DigestUtil.serialToCode(upperSerial);
		long parentCode = DigestUtil.serialToParentCode(upperSerial);
		long leftCode = DigestUtil.getLeftestCode(parentCode, upperCode);
		long leftSerial = DigestUtil.codeToSerial(leftCode);
		
		List<Long> parentCodes = new ArrayList<>();
		while (leftSerial != lowerSerial) {
			if (leftSerial > lowerSerial) {
				parentCodes.add(-parentCode);
				upperSerial = leftSerial - 1;
				upperCode = DigestUtil.serialToCode(upperSerial);
				parentCode = DigestUtil.serialToParentCode(upperSerial);
			}
			else {
				--parentCode;
			}
			leftCode = DigestUtil.getLeftestCode(parentCode, upperCode);
			leftSerial = DigestUtil.codeToSerial(leftCode);
		}
		parentCodes.add(-parentCode);
		Long[] codeArray = parentCodes.toArray(new Long[0]);
		T[] parenetDigests = (T[]) reader.getDigests(rowkey, codeArray);
		return parenetDigests;
	}

	public void setMaxSerialNo(long maxSerialNo) {
		this.maxSerialNo = maxSerialNo;
	}

	public ForestRootStack<T> getRootNodes() {
		return rootNodes;
	}

	public void setRootNodes(ForestRootStack<T> rootNodes) {
		this.rootNodes = rootNodes;
	}
	
	
}